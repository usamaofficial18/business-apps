version: '3.7'

services:
  # Frontend
  authorization-client:
    image: registry.gitlab.com/castlecraft/building-blocks/authorization-client:edge
    environment:
      - API_HOST=authorization-server
      - API_PORT=3000
    ports:
      - 4210:8080

  admin-client:
    image: registry.gitlab.com/castlecraft/building-blocks/admin-client:edge
    environment:
      - API_HOST=infrastructure-console
      - API_PORT=5000
    ports:
      - 4220:8080

  identity-client:
    image: registry.gitlab.com/castlecraft/building-blocks/identity-client:edge
    environment:
      - API_HOST=identity-provider
      - API_PORT=3200
    ports:
      - 4420:8080

  # Backend
  authorization-server:
    image: registry.gitlab.com/castlecraft/building-blocks/authorization-server:edge
    extra_hosts:
      admin.localhost: 172.17.0.1
      connect.localhost: 172.17.0.1
      myaccount.localhost: 172.17.0.1
    environment:
      - NODE_ENV=development
      - SESSION_SECRET=admin
      - COOKIE_MAX_AGE=7.884e+9
      - SESSION_NAME=bb-io
      - TOKEN_VALIDITY=3600
      - DB_HOST=mongo
      - DB_NAME=authorization-server
      - DB_USER=authorization-server
      - DB_PASSWORD=admin
      - EVENTS_HOST=event-bus
      - EVENTS_USER=user
      - EVENTS_PASSWORD=admin
      - EVENTS_PORT=1883
      - EVENTS_PROTO=mqtt

  communication-server:
    image: registry.gitlab.com/castlecraft/building-blocks/communication-server:edge
    extra_hosts:
      accounts.localhost: 172.17.0.1
      admin.localhost: 172.17.0.1
      myaccount.localhost: 172.17.0.1
    environment:
      - NODE_ENV=development
      - DB_HOST=mongo
      - DB_NAME=communication-server
      - DB_USER=communication-server
      - DB_PASSWORD=admin
      - EVENTS_HOST=event-bus
      - EVENTS_USER=user
      - EVENTS_PASSWORD=admin
      - EVENTS_PORT=1883
      - EVENTS_PROTO=mqtt
    ports:
      - 4100:4100

  identity-provider:
    image: registry.gitlab.com/castlecraft/building-blocks/identity-provider:edge
    extra_hosts:
      accounts.localhost: 172.17.0.1
      admin.localhost: 172.17.0.1
      connect.localhost: 172.17.0.1
    environment:
      - NODE_ENV=development
      - DB_HOST=mongo
      - DB_NAME=identity-provider
      - DB_USER=identity-provider
      - DB_PASSWORD=admin
      - EVENTS_HOST=event-bus
      - EVENTS_USER=user
      - EVENTS_PASSWORD=admin
      - EVENTS_PORT=1883
      - EVENTS_PROTO=mqtt

  infrastructure-console:
    extra_hosts:
      accounts.localhost: 172.17.0.1
      connect.localhost: 172.17.0.1
      myaccount.localhost: 172.17.0.1
    image: registry.gitlab.com/castlecraft/building-blocks/infrastructure-console:edge
    environment:
      - NODE_ENV=development
      - DB_HOST=mongo
      - DB_NAME=infrastructure-console
      - DB_USER=infrastructure-console
      - DB_PASSWORD=admin
      - EVENTS_HOST=event-bus
      - EVENTS_USER=user
      - EVENTS_PASSWORD=admin
      - EVENTS_PORT=1883
      - EVENTS_PROTO=mqtt

  # Services
  event-bus:
    restart: always
    image: emqx/emqx:latest
    environment:
      - EMQX_ALLOW_ANONYMOUS=true
      - EMQX_LOADED_PLUGINS=emqx_management,emqx_auth_mnesia,emqx_recon,emqx_retainer,emqx_dashboard
    ports:
      - 1883:1883
      - 18083:18083
      - 8081:8081

  mongo:
    restart: always
    image: bitnami/mongodb:latest
    environment:
      - "MONGODB_ROOT_PASSWORD=admin"
      - "MONGODB_DATABASE=authorization-server"
      - "MONGODB_USERNAME=authorization-server"
      - "MONGODB_PASSWORD=admin"
    volumes:
      - mongodb-vol:/bitnami/mongodb
    ports:
      - 27017:27017

  mongo-configuration:
    image: bitnami/mongodb:latest
    command:
      - bash
      - -c
      - >
        sleep 10;
        mongo identity-provider \
          --host mongo \
          --port 27017 \
          -u root \
          -p admin \
          --authenticationDatabase admin \
          --eval "db.createUser({user: 'identity-provider', pwd: 'admin', roles:[{role:'dbOwner', db: 'identity-provider'}]});";

        mongo infrastructure-console \
          --host mongo \
          --port 27017 \
          -u root \
          -p admin \
          --authenticationDatabase admin \
          --eval "db.createUser({user: 'infrastructure-console', pwd: 'admin', roles:[{role:'dbOwner', db: 'infrastructure-console'}]});";

        mongo communication-server \
          --host mongo \
          --port 27017 \
          -u root \
          -p admin \
          --authenticationDatabase admin \
          --eval "db.createUser({user: 'communication-server', pwd: 'admin', roles:[{role:'dbOwner', db: 'communication-server'}]});";

        mongo test_authorization-server \
          --host mongo \
          --port 27017 \
          -u root \
          -p admin \
          --authenticationDatabase admin \
          --eval "db.createUser({user: 'authorization-server', pwd: 'admin', roles:[{role:'dbOwner', db: 'test_authorization-server'}]});"

        mongo tokendb \
          --host mongo \
          --port 27017 \
          -u root \
          -p admin \
          --authenticationDatabase admin \
          --eval "db.createUser({user: 'tokenuser', pwd: 'admin', roles:[{role:'dbOwner', db: 'tokendb'}], passwordDigestor: 'server'});"

        mongo eventsdb \
          --host mongo \
          --port 27017 \
          -u root \
          -p admin \
          --authenticationDatabase admin \
          --eval "db.createUser({user: 'eventsuser', pwd: 'admin', roles:[{role:'dbOwner', db: 'eventsdb'}], passwordDigestor: 'server'});"

        mongo accountsdb \
          --host mongo \
          --port 27017 \
          -u root \
          -p admin \
          --authenticationDatabase admin \
          --eval "db.createUser({user: 'accountsuser', pwd: 'admin', roles:[{role:'dbOwner', db: 'accountsdb'}], passwordDigestor: 'server'});"

  business-apps:
    extra_hosts:
      accounts.localhost: 172.17.0.1
      admin.localhost: 172.17.0.1
      connect.localhost: 172.17.0.1
      myaccount.localhost: 172.17.0.1
    build: .
    volumes:
      - ..:/workspace
    working_dir: /workspace
    ports:
      - '8000:8000'
      - '7000:7000'

volumes:
  mongodb-vol:
