import { DynamicModule } from '@nestjs/common';
import { AsyncIdentityDatabaseOptions } from './common';
export declare class AuthModule {
    static registerAsync(options: AsyncIdentityDatabaseOptions): DynamicModule;
    private static createAsyncProviders;
    private static createAsyncOptionsProvider;
}
