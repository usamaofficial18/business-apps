"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClientAuthentication = void 0;
var ClientAuthentication;
(function (ClientAuthentication) {
    ClientAuthentication["BasicHeader"] = "BASIC_HEADER";
    ClientAuthentication["BodyParam"] = "BODY_PARAM";
    ClientAuthentication["PublicClient"] = "PUBLIC_CLIENT";
})(ClientAuthentication = exports.ClientAuthentication || (exports.ClientAuthentication = {}));
//# sourceMappingURL=client.interface.js.map