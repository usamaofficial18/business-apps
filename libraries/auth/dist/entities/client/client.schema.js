"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CLIENT = exports.Client = exports.randomBytes32 = void 0;
const crypto_1 = require("crypto");
const mongoose = require("mongoose");
const uuid_1 = require("uuid");
const client_interface_1 = require("./client.interface");
function randomBytes32() {
    return crypto_1.randomBytes(32).toString('hex');
}
exports.randomBytes32 = randomBytes32;
const schema = new mongoose.Schema({
    uuid: { type: String, default: uuid_1.v4 },
    creation: Date,
    modified: Date,
    createdBy: String,
    modifiedBy: String,
    name: String,
    clientId: { type: String, unique: true, sparse: true, index: true },
    clientSecret: String,
    isTrusted: Number,
    autoApprove: Boolean,
    redirectUris: [String],
    allowedScopes: [String],
    userDeleteEndpoint: String,
    tokenDeleteEndpoint: String,
    changedClientSecret: String,
    authenticationMethod: {
        type: String,
        default: client_interface_1.ClientAuthentication.PublicClient,
    },
}, { collection: 'token_client', versionKey: false });
exports.Client = schema;
exports.CLIENT = 'Client';
//# sourceMappingURL=client.schema.js.map