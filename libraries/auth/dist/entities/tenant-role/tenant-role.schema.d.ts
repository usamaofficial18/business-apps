import * as mongoose from 'mongoose';
export declare const TenantRole: mongoose.Schema<mongoose.Document<any, any, any>, mongoose.Model<mongoose.Document<any, any, any>, any, any>, undefined, {}>;
export declare const TENANT_ROLE = "TenantRole";
