import * as mongoose from 'mongoose';
export declare const TenantUser: mongoose.Schema<mongoose.Document<any, any, any>, mongoose.Model<mongoose.Document<any, any, any>, any, any>, undefined, {}>;
export declare const TENANT_USER = "TenantUser";
