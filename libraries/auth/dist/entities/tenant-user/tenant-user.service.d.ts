import { FilterQuery, InsertManyOptions, Model, QueryOptions } from 'mongoose';
import { TenantUser } from './tenant-user.interface';
export declare class TenantUserService {
    readonly model: Model<TenantUser>;
    constructor(model: Model<TenantUser>);
    insertOne(params: any): Promise<TenantUser>;
    insertMany(params: any, options?: InsertManyOptions): Promise<TenantUser[]>;
    findOne(filter?: FilterQuery<TenantUser>, projections?: any, options?: QueryOptions): Promise<TenantUser>;
    find(filter?: FilterQuery<TenantUser>, projections?: any, options?: QueryOptions): Promise<TenantUser[]>;
    updateOne(filter: FilterQuery<TenantUser>, update: any, options?: QueryOptions): Promise<import("mongoose").UpdateWriteOpResult>;
    updateMany(filter: FilterQuery<TenantUser>, update: any, options?: QueryOptions): Promise<import("mongoose").UpdateWriteOpResult>;
    deleteOne(filter: FilterQuery<TenantUser>, options?: QueryOptions): Promise<{
        ok?: number;
        n?: number;
    } & {
        deletedCount?: number;
    }>;
    deleteMany(filter: FilterQuery<TenantUser>, options?: QueryOptions): Promise<{
        ok?: number;
        n?: number;
    } & {
        deletedCount?: number;
    }>;
}
