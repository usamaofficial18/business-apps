import * as mongoose from 'mongoose';
export declare const TokenCache: mongoose.Schema<mongoose.Document<any, any, any>, mongoose.Model<mongoose.Document<any, any, any>, any, any>, undefined, {}>;
export declare const TOKEN_CACHE = "TokenCache";
