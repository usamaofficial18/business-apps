"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TOKEN_CACHE = exports.TokenCache = void 0;
const mongoose = require("mongoose");
const schema = new mongoose.Schema({
    uuid: String,
    creation: Date,
    modified: Date,
    createdBy: String,
    modifiedBy: String,
    accessToken: { type: String, index: true, unique: true, sparse: true },
    refreshToken: String,
    redirectUris: [String],
    scope: [String],
    expiresIn: Number,
    user: String,
    client: String,
    isTrustedClient: Boolean,
}, { collection: 'token_cache', versionKey: false });
exports.TokenCache = schema;
exports.TOKEN_CACHE = 'TokenCache';
//# sourceMappingURL=token-cache.schema.js.map