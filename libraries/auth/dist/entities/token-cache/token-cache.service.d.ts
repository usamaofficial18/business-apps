import { FilterQuery, InsertManyOptions, Model, QueryOptions } from 'mongoose';
import { TokenCache } from './token-cache.interface';
export declare class TokenCacheService {
    readonly model: Model<TokenCache>;
    constructor(model: Model<TokenCache>);
    insertOne(params: any): Promise<TokenCache>;
    insertMany(params: any, options?: InsertManyOptions): Promise<TokenCache[]>;
    findOne(filter?: FilterQuery<TokenCache>, projections?: any, options?: QueryOptions): Promise<TokenCache>;
    find(filter?: FilterQuery<TokenCache>, projections?: any, options?: QueryOptions): Promise<TokenCache[]>;
    updateOne(filter: FilterQuery<TokenCache>, update: any, options?: QueryOptions): Promise<import("mongoose").UpdateWriteOpResult>;
    updateMany(filter: FilterQuery<TokenCache>, update: any, options?: QueryOptions): Promise<import("mongoose").UpdateWriteOpResult>;
    deleteOne(filter: FilterQuery<TokenCache>, options?: QueryOptions): Promise<{
        ok?: number;
        n?: number;
    } & {
        deletedCount?: number;
    }>;
    deleteMany(filter: FilterQuery<TokenCache>, options?: QueryOptions): Promise<{
        ok?: number;
        n?: number;
    } & {
        deletedCount?: number;
    }>;
}
