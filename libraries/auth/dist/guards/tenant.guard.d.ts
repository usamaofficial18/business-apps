import { CanActivate, ExecutionContext } from '@nestjs/common';
import { TenantUserService } from '../entities/tenant-user/tenant-user.service';
export declare const X_TENANT_ID = "x-tenant-id";
export declare class TenantGuard implements CanActivate {
    private readonly tenantUser;
    constructor(tenantUser: TenantUserService);
    canActivate(context: ExecutionContext): Promise<boolean>;
}
