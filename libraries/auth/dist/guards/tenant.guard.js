"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TenantGuard = exports.X_TENANT_ID = void 0;
const common_1 = require("@nestjs/common");
const tenant_user_service_1 = require("../entities/tenant-user/tenant-user.service");
exports.X_TENANT_ID = 'x-tenant-id';
let TenantGuard = class TenantGuard {
    constructor(tenantUser) {
        this.tenantUser = tenantUser;
    }
    async canActivate(context) {
        var _a, _b;
        const req = (_a = context === null || context === void 0 ? void 0 : context.switchToHttp()) === null || _a === void 0 ? void 0 : _a.getRequest();
        const tenant = req.header(exports.X_TENANT_ID);
        const tenantUser = await this.tenantUser.findOne({
            tenant,
            user: (_b = req.user) === null || _b === void 0 ? void 0 : _b.uuid,
        });
        if (tenantUser) {
            req.tenantUser = tenantUser;
            return true;
        }
        return false;
    }
};
TenantGuard = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [tenant_user_service_1.TenantUserService])
], TenantGuard);
exports.TenantGuard = TenantGuard;
//# sourceMappingURL=tenant.guard.js.map