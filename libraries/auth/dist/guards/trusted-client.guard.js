"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrustedClientGuard = void 0;
const common_1 = require("@nestjs/common");
let TrustedClientGuard = class TrustedClientGuard {
    async canActivate(context) {
        const httpContext = context.switchToHttp();
        const req = httpContext.getRequest();
        const client = req.tokenClient;
        return (client === null || client === void 0 ? void 0 : client.isTrusted) > 0 ? true : false;
    }
};
TrustedClientGuard = __decorate([
    common_1.Injectable()
], TrustedClientGuard);
exports.TrustedClientGuard = TrustedClientGuard;
//# sourceMappingURL=trusted-client.guard.js.map