import { CanActivate, ExecutionContext } from '@nestjs/common';
import { UserService } from '../entities/user/user.service';
export declare const USER_KEY = "user";
export declare class UserGuard implements CanActivate {
    private readonly user;
    constructor(user: UserService);
    canActivate(context: ExecutionContext): Promise<boolean>;
}
