import { Test, TestingModule } from '@nestjs/testing';
import { TENANT_ROLE } from './tenant-role.schema';
import { TenantRoleService } from './tenant-role.service';

describe('TenantRoleService', () => {
  let service: TenantRoleService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TenantRoleService,
        {
          provide: TENANT_ROLE,
          useValue: {}, // provide mock values
        },
      ],
    }).compile();
    service = module.get<TenantRoleService>(TenantRoleService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
