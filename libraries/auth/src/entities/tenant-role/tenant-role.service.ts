import { Inject, Injectable } from '@nestjs/common';
import { FilterQuery, InsertManyOptions, Model, QueryOptions } from 'mongoose';
import { TenantRole } from './tenant-role.interface';
import { TENANT_ROLE } from './tenant-role.schema';

@Injectable()
export class TenantRoleService {
  constructor(@Inject(TENANT_ROLE) public readonly model: Model<TenantRole>) {}

  public async insertOne(params) {
    return await this.model.create(params);
  }

  public async insertMany(params, options?: InsertManyOptions) {
    return await this.model.insertMany(params, options);
  }

  public async findOne(
    filter?: FilterQuery<TenantRole>,
    projections?,
    options?: QueryOptions,
  ): Promise<TenantRole> {
    return await this.model.findOne(filter, projections, options);
  }

  async find(
    filter?: FilterQuery<TenantRole>,
    projections?,
    options?: QueryOptions,
  ): Promise<TenantRole[]> {
    return await this.model.find(filter, projections, options).exec();
  }

  public async updateOne(
    filter: FilterQuery<TenantRole>,
    update,
    options?: QueryOptions,
  ) {
    return await this.model.updateOne(filter, update, options);
  }

  public async updateMany(
    filter: FilterQuery<TenantRole>,
    update,
    options?: QueryOptions,
  ) {
    return await this.model.updateMany(filter, update, options);
  }

  public async deleteOne(
    filter: FilterQuery<TenantRole>,
    options?: QueryOptions,
  ) {
    return await this.model.deleteOne(filter, options);
  }

  public async deleteMany(
    filter: FilterQuery<TenantRole>,
    options?: QueryOptions,
  ) {
    return await this.model.deleteMany(filter, options);
  }
}
