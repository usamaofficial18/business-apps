import { Test, TestingModule } from '@nestjs/testing';
import { TenantService } from './tenant.service';
import { TENANT } from './tenant.schema';

describe('TenantService', () => {
  let service: TenantService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TenantService,
        {
          provide: TENANT,
          useValue: {}, // provide mock values
        },
      ],
    }).compile();
    service = module.get<TenantService>(TenantService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
