import { Inject, Injectable } from '@nestjs/common';
import { FilterQuery, InsertManyOptions, Model, QueryOptions } from 'mongoose';
import { Tenant } from './tenant.interface';
import { TENANT } from './tenant.schema';

@Injectable()
export class TenantService {
  constructor(@Inject(TENANT) public readonly model: Model<Tenant>) {}

  public async insertOne(params) {
    return await this.model.create(params);
  }

  public async insertMany(params, options?: InsertManyOptions) {
    return await this.model.insertMany(params, options);
  }

  public async findOne(
    filter?: FilterQuery<Tenant>,
    projections?,
    options?: QueryOptions,
  ): Promise<Tenant> {
    return await this.model.findOne(filter, projections, options);
  }

  async find(
    filter?: FilterQuery<Tenant>,
    projections?,
    options?: QueryOptions,
  ): Promise<Tenant[]> {
    return await this.model.find(filter, projections, options).exec();
  }

  public async updateOne(
    filter: FilterQuery<Tenant>,
    update,
    options?: QueryOptions,
  ) {
    return await this.model.updateOne(filter, update, options);
  }

  public async updateMany(
    filter: FilterQuery<Tenant>,
    update,
    options?: QueryOptions,
  ) {
    return await this.model.updateMany(filter, update, options);
  }

  public async deleteOne(filter: FilterQuery<Tenant>, options?: QueryOptions) {
    return await this.model.deleteOne(filter, options);
  }

  public async deleteMany(filter: FilterQuery<Tenant>, options?: QueryOptions) {
    return await this.model.deleteMany(filter, options);
  }
}
