import { Test, TestingModule } from '@nestjs/testing';
import { TOKEN_CACHE } from './token-cache.schema';
import { TokenCacheService } from './token-cache.service';

describe('TokenCacheService', () => {
  let service: TokenCacheService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TokenCacheService,
        {
          provide: TOKEN_CACHE,
          useValue: {}, // provide mock values
        },
      ],
    }).compile();
    service = module.get<TokenCacheService>(TokenCacheService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
