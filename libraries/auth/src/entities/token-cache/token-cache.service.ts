import { Inject, Injectable } from '@nestjs/common';
import { FilterQuery, InsertManyOptions, Model, QueryOptions } from 'mongoose';
import { TokenCache } from './token-cache.interface';
import { TOKEN_CACHE } from './token-cache.schema';

@Injectable()
export class TokenCacheService {
  constructor(
    @Inject(TOKEN_CACHE)
    public readonly model: Model<TokenCache>,
  ) {}

  public async insertOne(params) {
    return await this.model.create(params);
  }

  public async insertMany(params, options?: InsertManyOptions) {
    return await this.model.insertMany(params, options);
  }

  public async findOne(
    filter?: FilterQuery<TokenCache>,
    projections?,
    options?: QueryOptions,
  ): Promise<TokenCache> {
    return await this.model.findOne(filter, projections, options);
  }

  async find(
    filter?: FilterQuery<TokenCache>,
    projections?,
    options?: QueryOptions,
  ): Promise<TokenCache[]> {
    return await this.model.find(filter, projections, options).exec();
  }

  public async updateOne(
    filter: FilterQuery<TokenCache>,
    update,
    options?: QueryOptions,
  ) {
    return await this.model.updateOne(filter, update, options);
  }

  public async updateMany(
    filter: FilterQuery<TokenCache>,
    update,
    options?: QueryOptions,
  ) {
    return await this.model.updateMany(filter, update, options);
  }

  public async deleteOne(
    filter: FilterQuery<TokenCache>,
    options?: QueryOptions,
  ) {
    return await this.model.deleteOne(filter, options);
  }

  public async deleteMany(
    filter: FilterQuery<TokenCache>,
    options?: QueryOptions,
  ) {
    return await this.model.deleteMany(filter, options);
  }
}
