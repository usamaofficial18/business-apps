import { Inject, Injectable } from '@nestjs/common';
import { FilterQuery, InsertManyOptions, Model, QueryOptions } from 'mongoose';
import { User } from './user.interface';
import { USER } from './user.schema';

@Injectable()
export class UserService {
  constructor(@Inject(USER) public readonly model: Model<User>) {}

  public async insertOne(params) {
    return await this.model.create(params);
  }

  public async insertMany(params, options?: InsertManyOptions) {
    return await this.model.insertMany(params, options);
  }

  public async findOne(
    filter?: FilterQuery<User>,
    projections?,
    options?: QueryOptions,
  ): Promise<User> {
    return await this.model.findOne(filter, projections, options);
  }

  async find(
    filter?: FilterQuery<User>,
    projections?,
    options?: QueryOptions,
  ): Promise<User[]> {
    return await this.model.find(filter, projections, options).exec();
  }

  public async updateOne(
    filter: FilterQuery<User>,
    update,
    options?: QueryOptions,
  ) {
    return await this.model.updateOne(filter, update, options);
  }

  public async updateMany(
    filter: FilterQuery<User>,
    update,
    options?: QueryOptions,
  ) {
    return await this.model.updateMany(filter, update, options);
  }

  public async deleteOne(filter: FilterQuery<User>, options?: QueryOptions) {
    return await this.model.deleteOne(filter, options);
  }

  public async deleteMany(filter: FilterQuery<User>, options?: QueryOptions) {
    return await this.model.deleteMany(filter, options);
  }
}
