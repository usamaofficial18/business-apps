import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ClientService } from '../entities/client/client.service';

@Injectable()
export class ClientGuard implements CanActivate {
  constructor(private readonly client: ClientService) {}

  async canActivate(context: ExecutionContext) {
    const httpContext = context.switchToHttp();
    const req = httpContext.getRequest();
    const client = await this.client.findOne({ clientId: req.token?.client });
    if (client) {
      req.tokenClient = client;
      return true;
    }
    return false;
  }
}
