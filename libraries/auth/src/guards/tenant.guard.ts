import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { TenantUserService } from '../entities/tenant-user/tenant-user.service';

export const X_TENANT_ID = 'x-tenant-id';

@Injectable()
export class TenantGuard implements CanActivate {
  constructor(private readonly tenantUser: TenantUserService) {}

  async canActivate(context: ExecutionContext) {
    const req = context?.switchToHttp()?.getRequest();
    const tenant = req.header(X_TENANT_ID);

    const tenantUser = await this.tenantUser.findOne({
      tenant,
      user: req.user?.uuid,
    });

    if (tenantUser) {
      req.tenantUser = tenantUser;
      return true;
    }

    return false;
  }
}
