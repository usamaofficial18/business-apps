import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { TokenCacheService } from '../entities/token-cache/token-cache.service';

export const TOKEN_KEY = 'token';

@Injectable()
export class TokenGuard implements CanActivate {
  constructor(private readonly tokenCacheService: TokenCacheService) {}

  async canActivate(context: ExecutionContext) {
    const httpContext = context.switchToHttp();
    const req = httpContext.getRequest();
    const accessToken = this.getAccessToken(req);
    const cachedToken = await this.tokenCacheService.findOne({ accessToken });

    if (!cachedToken) {
      return false;
    }

    const now = new Date();
    const expiry = cachedToken.creation;
    expiry.setSeconds(expiry.getSeconds() + cachedToken.expiresIn);
    if (now < expiry) {
      req[TOKEN_KEY] = cachedToken;
      return true;
    }
    return false;
  }

  getAccessToken(request) {
    if (!request.headers.authorization) {
      if (!request.query.access_token) return null;
    }
    return (
      request.query.access_token ||
      request.headers.authorization.split(' ')[1] ||
      null
    );
  }
}
