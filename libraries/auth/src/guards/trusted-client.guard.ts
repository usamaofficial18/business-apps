import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Client } from '../entities/client/client.interface';

@Injectable()
export class TrustedClientGuard implements CanActivate {
  async canActivate(context: ExecutionContext) {
    const httpContext = context.switchToHttp();
    const req = httpContext.getRequest();
    const client: Client = req.tokenClient;

    return client?.isTrusted > 0 ? true : false;
  }
}
