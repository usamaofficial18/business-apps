import { Inject, Injectable } from '@nestjs/common';
import { Connection } from 'mongoose';
import { IDENTITY_CONNECTION } from '../common';

@Injectable()
export class AuthDbConnectionService {
  constructor(
    @Inject(IDENTITY_CONNECTION)
    public readonly connection: Connection,
  ) {}
}
