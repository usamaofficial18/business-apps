import { Document } from 'mongoose';
export interface DomainEvent extends Document {
    eventId: string;
    eventName: string;
    eventFromService: string;
    eventDateTime: Date;
    eventData: any;
}
