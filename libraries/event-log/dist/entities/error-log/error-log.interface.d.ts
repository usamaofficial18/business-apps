import { Document } from 'mongoose';
export interface ErrorLog extends Document {
    uuid: string;
    message: string;
    fromService: string;
    dateTime: Date;
    data: any;
}
