import { DynamicModule } from '@nestjs/common';
import { AsyncEventsDatabaseOptions } from './common';
export declare class EventLogModule {
    static registerAsync(options: AsyncEventsDatabaseOptions): DynamicModule;
    private static createAsyncProviders;
    private static createAsyncOptionsProvider;
}
