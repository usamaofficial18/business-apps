import { Logger } from '@nestjs/common';
import { Observable } from 'rxjs';
import { delay, retryWhen, scan } from 'rxjs/operators';

export const EVENTLOG_CONNECTION = 'EVENTLOG_CONNECTION';
export const EventsProvider = 'EventsProvider';
export const EVENTLOG_MODULE_OPTIONS = 'EVENTLOG_MODULE_OPTIONS';

export interface EventsDatabaseOptions {
  mongoUriPrefix: string;
  eventsDbHost: string;
  eventsDbUser: string;
  eventsDbPassword: string;
  eventsDbName: string;
}

export interface AsyncEventsDatabaseOptions {
  inject: any[];
  useFactory: (...args) => EventsDatabaseOptions;
}

export function handleRetry(
  providerName: string,
  retryAttempts = 9,
  retryDelay = 3000,
): <T>(source: Observable<T>) => Observable<T> {
  return <T>(source: Observable<T>) =>
    source.pipe(
      retryWhen(e =>
        e.pipe(
          scan((errorCount, error) => {
            Logger.error(
              error,
              `Unable to connect to the database. Retrying (${
                errorCount + 1
              })...`,
              providerName,
            );
            if (errorCount + 1 >= retryAttempts) {
              throw error;
            }
            return errorCount + 1;
          }, 0),
          delay(retryDelay),
        ),
      ),
    );
}
