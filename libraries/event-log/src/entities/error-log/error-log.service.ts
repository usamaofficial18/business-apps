import { Inject, Injectable } from '@nestjs/common';
import { FilterQuery, InsertManyOptions, Model, QueryOptions } from 'mongoose';
import { ErrorLog } from './error-log.interface';
import { ERROR_LOG } from './error-log.schema';

@Injectable()
export class ErrorLogService {
  constructor(@Inject(ERROR_LOG) public readonly model: Model<ErrorLog>) {}

  public async insertOne(params) {
    return await this.model.create(params);
  }

  public async insertMany(params, options?: InsertManyOptions) {
    return await this.model.insertMany(params, options);
  }

  public async findOne(
    filter?: FilterQuery<ErrorLog>,
    projections?,
    options?: QueryOptions,
  ): Promise<ErrorLog> {
    return await this.model.findOne(filter, projections, options);
  }

  async find(
    filter?: FilterQuery<ErrorLog>,
    projections?,
    options?: QueryOptions,
  ): Promise<ErrorLog[]> {
    return await this.model.find(filter, projections, options).exec();
  }

  public async updateOne(
    filter: FilterQuery<ErrorLog>,
    update,
    options?: QueryOptions,
  ) {
    return await this.model.updateOne(filter, update, options);
  }

  public async updateMany(
    filter: FilterQuery<ErrorLog>,
    update,
    options?: QueryOptions,
  ) {
    return await this.model.updateMany(filter, update, options);
  }

  public async deleteOne(
    filter: FilterQuery<ErrorLog>,
    options?: QueryOptions,
  ) {
    return await this.model.deleteOne(filter, options);
  }

  public async deleteMany(
    filter: FilterQuery<ErrorLog>,
    options?: QueryOptions,
  ) {
    return await this.model.deleteMany(filter, options);
  }
}
