import { Provider } from '@nestjs/common';
import { Connection } from 'mongoose';
import { EVENTLOG_CONNECTION } from '../common';
import { DomainEvent, DOMAIN_EVENT } from './domain-event/domain-event.schema';
import { DomainEventService } from './domain-event/domain-event.service';
import { ErrorLog, ERROR_LOG } from './error-log/error-log.schema';
import { ErrorLogService } from './error-log/error-log.service';

export const EventsEntities: Provider[] = [
  {
    provide: ERROR_LOG,
    useFactory: (connection: Connection) =>
      connection.model(ERROR_LOG, ErrorLog),
    inject: [EVENTLOG_CONNECTION],
  },
  {
    provide: DOMAIN_EVENT,
    useFactory: (connection: Connection) =>
      connection.model(DOMAIN_EVENT, DomainEvent),
    inject: [EVENTLOG_CONNECTION],
  },
];

export const EventsEntityServices: Provider[] = [
  DomainEventService,
  ErrorLogService,
];
