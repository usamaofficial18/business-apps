import { DynamicModule, Module, Provider } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { defer, lastValueFrom } from 'rxjs';
import {
  AsyncEventsDatabaseOptions,
  EVENTLOG_CONNECTION,
  EVENTLOG_MODULE_OPTIONS,
  EventsDatabaseOptions,
  EventsProvider,
  handleRetry,
} from './common';
import { EventsEntities, EventsEntityServices } from './entities';
import { EventsDbConnectionService } from './services';

@Module({
  providers: [
    ...EventsEntities,
    ...EventsEntityServices,
    EventsDbConnectionService,
  ],
  exports: [...EventsEntityServices, EventsDbConnectionService],
})
export class EventLogModule {
  static registerAsync(options: AsyncEventsDatabaseOptions): DynamicModule {
    return {
      module: EventLogModule,
      providers: [...this.createAsyncProviders(options)],
    };
  }

  private static createAsyncProviders(
    options: AsyncEventsDatabaseOptions,
  ): Provider[] {
    const providers: Provider[] = [
      {
        provide: EVENTLOG_CONNECTION,
        useFactory: async (options: EventsDatabaseOptions) => {
          const mongoOptions = 'retryWrites=true';
          return await lastValueFrom(
            defer(() => {
              return mongoose.createConnection(
                `${options.mongoUriPrefix}://${options.eventsDbUser}:${
                  options.eventsDbPassword
                }@${options.eventsDbHost.replace(/,\s*$/, '')}/${
                  options.eventsDbName
                }?${mongoOptions}`,
                {
                  useNewUrlParser: true,
                  useUnifiedTopology: true,
                  autoReconnect: false,
                  reconnectTries: 0,
                  reconnectInterval: 0,
                  useCreateIndex: true,
                },
              );
            }).pipe(handleRetry(EventsProvider)),
          );
        },
        inject: [EVENTLOG_MODULE_OPTIONS],
      },
    ];
    return [this.createAsyncOptionsProvider(options), ...providers];
  }

  private static createAsyncOptionsProvider(
    options: AsyncEventsDatabaseOptions,
  ): Provider {
    return {
      provide: EVENTLOG_MODULE_OPTIONS,
      useFactory: options.useFactory,
      inject: options.inject,
    };
  }
}
