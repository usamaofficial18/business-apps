import { Test, TestingModule } from '@nestjs/testing';
import { AccountService } from '../../entities/account/account.service';
import { JournalEntryService } from '../../entities/journal-entry/journal-entry.service';
import { LedgerEntryService } from '../../entities/ledger-entry/ledger-entry.service';
import { AccountingEntityService } from '../../entities/accounting-entity/accounting-entity.service';
import { AccountingEntityAggregateService } from './accounting-entity-aggregate.service';

describe('AccountingEntityAggerageService', () => {
  let service: AccountingEntityAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AccountingEntityAggregateService,
        {
          provide: AccountingEntityService,
          useValue: {},
        },
        {
          provide: AccountService,
          useValue: {},
        },
        {
          provide: JournalEntryService,
          useValue: {},
        },
        {
          provide: LedgerEntryService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<AccountingEntityAggregateService>(
      AccountingEntityAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
