import { TenantUser } from '@castlecraft/auth';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { v4 as uuidv4 } from 'uuid';
import { stringEscape } from '../../../common/utils';
import {
  ACCOUNT_LINK_FOUND,
  JOURNAL_ENTRY_LINK_FOUND,
  LEDGER_ENTRY_LINK_FOUND,
} from '../../../constants/messages';
import { ListAccountingEntityDto } from '../../controllers/accounting-entity/list-accounting-entity.dto';
import { AccountService } from '../../entities/account/account.service';
import { AccountingEntityDto } from '../../entities/accounting-entity/accounting-entity.dto';
import { AccountingEntity } from '../../entities/accounting-entity/accounting-entity.interface';
import { AccountingEntityService } from '../../entities/accounting-entity/accounting-entity.service';
import { JournalEntryService } from '../../entities/journal-entry/journal-entry.service';
import { LedgerEntryService } from '../../entities/ledger-entry/ledger-entry.service';
import { AccountingEntityPurgedEvent } from '../../events/accounting-entity-purged/accounting-entity-purged.event';
import { AccountingEntityAddedEvent } from '../../events/accounting-entity/accounting-entity-added/accounting-entity-added.event';
import { AccountingEntityRemovedEvent } from '../../events/accounting-entity/accounting-entity-removed/accounting-entity-removed.event';
import { AccountingEntityUpdatedEvent } from '../../events/accounting-entity/accounting-entity-updated/accounting-entity-updated.event';

@Injectable()
export class AccountingEntityAggregateService extends AggregateRoot {
  constructor(
    private readonly accountingEntity: AccountingEntityService,
    private readonly accountService: AccountService,
    private readonly journalEntryService: JournalEntryService,
    private readonly ledgerEntryService: LedgerEntryService,
  ) {
    super();
  }

  async addAccountingEntity(
    accountingEntityPayload: AccountingEntityDto,
    tenantUser: TenantUser,
  ) {
    const accountingEntity = {} as AccountingEntity;
    Object.assign(accountingEntity, accountingEntityPayload);
    accountingEntity.uuid = uuidv4();
    accountingEntity.dateOfBirth = accountingEntityPayload.dateOfBirth
      ? new Date(accountingEntityPayload.dateOfBirth)
      : undefined;
    this.apply(new AccountingEntityAddedEvent(accountingEntity, tenantUser));

    return accountingEntity;
  }

  async getAccountingEntityList(
    query: ListAccountingEntityDto,
    tenantUser: TenantUser,
  ) {
    const { limit, offset, sort, ...filters } = query;
    const sortOrder = sort
      ? { [sort.split(' ')[0]]: sort.split(' ')[1] === 'asc' ? 1 : -1 }
      : { name: 1 };

    Object.keys(filters).forEach(key => {
      if (filters[key] === undefined) {
        delete filters[key];
      } else {
        try {
          filters[key] = new RegExp(stringEscape(filters[key]), 'i');
        } catch (error) {}
      }
    });

    const results = await this.accountingEntity.model
      .find({ ...filters, tenantId: tenantUser.tenant })
      .collation({ locale: 'en_US', numericOrdering: true })
      .limit(Number(limit || 10))
      .skip(Number(offset || 0))
      .sort(sortOrder);

    return {
      docs: results || [],
      length: await this.accountingEntity.model.countDocuments(filters),
      offset,
    };
  }

  async removeAccountingEntity(uuid: string) {
    const entity = await this.findOrFail(uuid);
    if (await this.accountService.findOne({ accountingEntity: uuid })) {
      throw new BadRequestException(ACCOUNT_LINK_FOUND);
    }
    if (await this.journalEntryService.findOne({ accountingEntity: uuid })) {
      throw new BadRequestException(JOURNAL_ENTRY_LINK_FOUND);
    }
    if (await this.ledgerEntryService.findOne({ accountingEntity: uuid })) {
      throw new BadRequestException(LEDGER_ENTRY_LINK_FOUND);
    }
    this.apply(new AccountingEntityRemovedEvent(entity));
  }

  async purgeAccountingEntity(uuid: string) {
    const accountingEntity = await this.findOrFail(uuid);
    this.apply(new AccountingEntityPurgedEvent(accountingEntity));
    return accountingEntity.toJSON();
  }

  async updateAccountingEntity(updatePayload: AccountingEntityDto) {
    const provider = await this.accountingEntity.findOne({
      uuid: updatePayload.uuid,
    });

    if (!provider) {
      throw new NotFoundException();
    }

    this.apply(new AccountingEntityUpdatedEvent(updatePayload));
  }

  async fetchEntity(uuid: string) {
    uuid = stringEscape(uuid);
    const entity = await this.accountingEntity.findOne({ uuid });
    if (!entity) {
      throw new NotFoundException({ AccountingEntityNotFound: uuid });
    }
    return entity;
  }

  async findOrFail(uuid: string) {
    const entity = await this.accountingEntity.findOne({ uuid });
    if (!entity) {
      throw new NotFoundException({ AccountingEntityNotFound: uuid });
    }
    return entity;
  }
}
