import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { from, of, throwError } from 'rxjs';
import { map, mergeMap, switchMap, toArray } from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';
import { AccountDto } from '../../entities/account/account-dto';
import { Account } from '../../entities/account/account.interface';
import { AccountService } from '../../entities/account/account.service';
import { AccountAddedEvent } from '../../events/account/account-added/account-added.event';
import { AccountRemovedEvent } from '../../events/account/account-removed/account-removed.event';
import { AccountUpdatedEvent } from '../../events/account/account-updated/account-updated.event';
import { ListAccountDto } from '../../controllers/account/list-account.dto';
import { CurrencyService } from '../../entities/currency/currency.service';

@Injectable()
export class AccountsAggregateService extends AggregateRoot {
  constructor(
    private readonly account: AccountService,
    private readonly currency: CurrencyService,
  ) {
    super();
  }

  async addAccount(accountPayload: AccountDto) {
    const accountNumber = accountPayload.accountNumber;
    const accountingEntity = accountPayload.accountingEntity;
    const accountExists = await this.account.findOne({
      accountNumber,
      accountingEntity,
    });
    if (accountExists) {
      return throwError(
        new BadRequestException({
          AccountAlreadyExistsFor: accountPayload.accountNumber,
        }),
      );
    }
    const currency = accountPayload.currency;
    const validCurrency = await this.currency.findOne({ currency });
    if (!validCurrency) {
      throw new NotFoundException();
    }

    const account = {} as Account;
    Object.assign(account, accountPayload);
    account.uuid = uuidv4();
    account.tenantId = accountPayload.tenantId;
    this.apply(new AccountAddedEvent(account));
    return of(account);
  }

  async getAccountsList(filter: ListAccountDto) {
    return await this.account.list(filter);
  }

  async removeAccount(uuid: string) {
    const account = await this.account.findOne({ uuid });
    if (!account) {
      throw new NotFoundException();
    }
    const childrenExists = await this.account.findOne({
      parentAccount: account.uuid,
    });
    if (childrenExists) {
      return throwError(
        new BadRequestException({
          ChildrenExistsFor: account.accountName,
        }),
      );
    }
    this.apply(new AccountRemovedEvent(account));
  }

  async updateAccount(updatePayload: AccountDto) {
    const provider = await this.account.findOne({
      uuid: updatePayload.uuid,
    });

    if (!provider) {
      throw new NotFoundException();
    }

    this.apply(new AccountUpdatedEvent(updatePayload));
  }

  loadRoot(accountingEntity: string) {
    return from(
      this.account.loadRoot({
        level: 0,
        accountingEntity,
      }),
    ).pipe(
      switchMap(accounts => from(accounts)),
      mergeMap(account => {
        return from(this.account.loadChildren(account.uuid)).pipe(
          map(children => ({
            ...account.toJSON(),
            children,
          })),
        );
      }),
      toArray(),
    );
  }

  loadChildren(uuid) {
    return from(this.account.loadChildren(uuid)).pipe(
      switchMap(accounts => from(accounts)),
      mergeMap(account => {
        return from(this.account.loadChildren(account.uuid)).pipe(
          map(children => ({
            ...account.toJSON(),
            children,
          })),
        );
      }),
      toArray(),
    );
  }
}
