import { Test, TestingModule } from '@nestjs/testing';
import { CurrencyService } from '../../../accounts/entities/currency/currency.service';
import { CurrencyAggregateService } from './currency-aggregate.service';

describe('CurrencyAggregateService', () => {
  let service: CurrencyAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CurrencyAggregateService,
        {
          provide: CurrencyService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<CurrencyAggregateService>(CurrencyAggregateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
