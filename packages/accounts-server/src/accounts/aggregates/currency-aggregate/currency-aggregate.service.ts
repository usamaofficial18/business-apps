import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as csv from 'fast-csv';
import * as fs from 'fs';
import * as path from 'path';
import { from, of } from 'rxjs';
import { fromStream } from '../../../common/utils';
import { ListCurrencyDto } from '../../../accounts/controllers/currency/list-currency.dto';
import { CurrencyDto } from '../../../accounts/entities/currency/currency-dto';
import { Currency } from '../../../accounts/entities/currency/currency.interface';
import { CurrencyService } from '../../../accounts/entities/currency/currency.service';
import { CurrencyAddedEvent } from '../../../accounts/events/currency/currency-added/currency-added.event';
import { CurrencyRemovedEvent } from '../../../accounts/events/currency/currency-removed/currency-removed.event';
import { CurrencyUpdatedEvent } from '../../../accounts/events/currency/currency-updated/currency-updated.event';
import { CURRENCY_ALREADY_EXISTS } from '../../../constants/messages';
import { mergeMap, toArray } from 'rxjs/operators';

@Injectable()
export class CurrencyAggregateService extends AggregateRoot {
  constructor(private readonly currency: CurrencyService) {
    super();
  }

  async addCurrency(currencyPayload: CurrencyDto) {
    if (
      await this.currency.findOne({
        code: currencyPayload.code,
      })
    ) {
      throw new BadRequestException(CURRENCY_ALREADY_EXISTS);
    }

    const currency = {} as Currency;
    Object.assign(currency, currencyPayload);
    currency.name = currencyPayload.name;
    currency.code = currencyPayload.code.toUpperCase();
    currency.minorUnit = Number(currencyPayload.minorUnit);
    this.apply(new CurrencyAddedEvent(currency));
    return of(currency);
  }

  async getCurrencyList(query: ListCurrencyDto) {
    return await this.currency.list(query);
  }

  async retrieveCurrency(code: string) {
    const currency = await this.currency.findOne({ code });
    if (!currency) {
      throw new NotFoundException({ CurrencyNotFound: code });
    }
    return currency;
  }

  async updateCurrency(updatePayload: CurrencyDto) {
    const provider = await this.currency.findOne({
      code: updatePayload.code,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    this.apply(new CurrencyUpdatedEvent(updatePayload));
  }

  async removeCurrency(code: string) {
    const currency = await this.currency.findOne({ code });
    if (!currency) {
      throw new NotFoundException();
    }
    this.apply(new CurrencyRemovedEvent(currency));
  }

  updateCurrenciesData() {
    return fromStream(
      fs
        .createReadStream(path.resolve('data', 'currencies-iso4217.csv'))
        .pipe(csv.parse({ headers: true })),
    ).pipe(
      mergeMap((currency: { code: string; name: string; minorUint: number }) =>
        from(
          this.currency.model.updateOne(
            { code: currency.code },
            { $set: currency },
            { upsert: true },
          ),
        ),
      ),
      toArray(),
    );
  }
}
