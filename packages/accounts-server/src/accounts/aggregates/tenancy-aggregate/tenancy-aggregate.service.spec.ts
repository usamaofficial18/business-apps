import { Test, TestingModule } from '@nestjs/testing';
import { AccountingEntityService } from '../../entities/accounting-entity/accounting-entity.service';
import { TenancyAggregateService } from './tenancy-aggregate.service';

describe('TenancyAggregateService', () => {
  let service: TenancyAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TenancyAggregateService,
        { provide: AccountingEntityService, useValue: {} },
      ],
    }).compile();

    service = module.get<TenancyAggregateService>(TenancyAggregateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
