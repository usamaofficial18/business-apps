import { Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { TenantEntitiesPurgedEvent } from '../../events/tenant-entities-purged/tenant-entities-purged.event';
import { AccountingEntityService } from '../../entities/accounting-entity/accounting-entity.service';

@Injectable()
export class TenancyAggregateService extends AggregateRoot {
  constructor(private readonly accountingEntity: AccountingEntityService) {
    super();
  }

  async purgeTenantResources(tenantId: string) {
    const accountingEntities = await this.accountingEntity.find({ tenantId });
    if (accountingEntities.length > 0) {
      this.apply(new TenantEntitiesPurgedEvent(tenantId));
    }
  }
}
