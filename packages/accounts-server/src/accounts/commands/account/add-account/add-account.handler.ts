import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AccountsAggregateService } from '../../../aggregates/accounts-aggregate/accounts-aggregate.service';
import { AddAccountCommand } from './add-account.command';

@CommandHandler(AddAccountCommand)
export class AddAccountHandler implements ICommandHandler<AddAccountCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: AccountsAggregateService,
  ) {}
  async execute(command: AddAccountCommand) {
    const { accountPayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const account = await aggregate.addAccount(accountPayload);
    aggregate.commit();
    return account;
  }
}
