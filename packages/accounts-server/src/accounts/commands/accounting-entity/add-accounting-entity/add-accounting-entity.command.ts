import { ICommand } from '@nestjs/cqrs';
import { TenantUser } from '@castlecraft/auth';
import { AccountingEntityDto } from '../../../entities/accounting-entity/accounting-entity.dto';

export class AddAccountingEntityCommand implements ICommand {
  constructor(
    public accountingEntityPayload: AccountingEntityDto,
    public readonly tenantUser: TenantUser,
  ) {}
}
