import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AccountingEntityAggregateService } from '../../../aggregates/accounting-entity-aggregate/accounting-entity-aggregate.service';
import { AddAccountingEntityCommand } from './add-accounting-entity.command';

@CommandHandler(AddAccountingEntityCommand)
export class AddAccountingEntityHandler
  implements ICommandHandler<AddAccountingEntityCommand>
{
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: AccountingEntityAggregateService,
  ) {}

  async execute(command: AddAccountingEntityCommand) {
    const { accountingEntityPayload, tenantUser } = command;
    accountingEntityPayload.tenantId = tenantUser?.tenant;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const accountingEntity = await aggregate.addAccountingEntity(
      accountingEntityPayload,
      tenantUser,
    );
    aggregate.commit();
    return accountingEntity;
  }
}
