import { ICommand } from '@nestjs/cqrs';

export class RemoveAccountingEntityCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
