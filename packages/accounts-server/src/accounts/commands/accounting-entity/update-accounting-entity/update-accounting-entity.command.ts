import { ICommand } from '@nestjs/cqrs';
import { AccountingEntityDto } from '../../../entities/accounting-entity/accounting-entity.dto';

export class UpdateAccountingEntityCommand implements ICommand {
  constructor(public readonly updatePayload: AccountingEntityDto) {}
}
