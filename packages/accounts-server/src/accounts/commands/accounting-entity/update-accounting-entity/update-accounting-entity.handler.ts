import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AccountingEntityAggregateService } from '../../../aggregates/accounting-entity-aggregate/accounting-entity-aggregate.service';
import { UpdateAccountingEntityCommand } from './update-accounting-entity.command';

@CommandHandler(UpdateAccountingEntityCommand)
export class UpdateAccountingEntityHandler
  implements ICommandHandler<UpdateAccountingEntityCommand>
{
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: AccountingEntityAggregateService,
  ) {}

  async execute(command: UpdateAccountingEntityCommand) {
    const { updatePayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.updateAccountingEntity(updatePayload);
    aggregate.commit();
    return updatePayload;
  }
}
