import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { JournalEntryAggregateService } from '../../../aggregates/journal-entry-aggregate/journal-entry-aggregate.service';
import { AddJournalEntryCommand } from './add-journal-entry.command';

@CommandHandler(AddJournalEntryCommand)
export class AddJournalEntryHandler
  implements ICommandHandler<AddJournalEntryCommand>
{
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: JournalEntryAggregateService,
  ) {}

  async execute(command: AddJournalEntryCommand) {
    const { journalEntryPayload, tenantUser } = command;
    journalEntryPayload.tenantId = tenantUser?.tenant;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const journalEntry = await aggregate.addJournalEntry(journalEntryPayload);
    aggregate.commit();
    return journalEntry;
  }
}
