import { ICommand } from '@nestjs/cqrs';

export class RemoveJournalEntryCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
