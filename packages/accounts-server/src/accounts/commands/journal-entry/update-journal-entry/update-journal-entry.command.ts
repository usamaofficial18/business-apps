import { ICommand } from '@nestjs/cqrs';
import { JournalEntryDto } from '../../../entities/journal-entry/journal-entry.dto';

export class UpdateJournalEntryCommand implements ICommand {
  constructor(public readonly updatePayload: JournalEntryDto) {}
}
