import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { JournalEntryAggregateService } from '../../../aggregates/journal-entry-aggregate/journal-entry-aggregate.service';
import { UpdateJournalEntryCommand } from './update-journal-entry.command';

@CommandHandler(UpdateJournalEntryCommand)
export class UpdateJournalEntryHandler
  implements ICommandHandler<UpdateJournalEntryCommand>
{
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: JournalEntryAggregateService,
  ) {}

  async execute(command: UpdateJournalEntryCommand) {
    const { updatePayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.updateJournalEntry(updatePayload);
    aggregate.commit();
    return updatePayload;
  }
}
