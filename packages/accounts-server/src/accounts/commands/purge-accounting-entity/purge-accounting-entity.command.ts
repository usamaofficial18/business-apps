import { ICommand } from '@nestjs/cqrs';

export class PurgeAccountingEntityCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
