import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AccountingEntityAggregateService } from '../../aggregates/accounting-entity-aggregate/accounting-entity-aggregate.service';
import { PurgeAccountingEntityCommand } from './purge-accounting-entity.command';

@CommandHandler(PurgeAccountingEntityCommand)
export class PurgeAccountingEntityHandler
  implements ICommandHandler<PurgeAccountingEntityCommand>
{
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: AccountingEntityAggregateService,
  ) {}

  async execute(command: PurgeAccountingEntityCommand) {
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const entity = await aggregate.purgeAccountingEntity(command.uuid);
    aggregate.commit();
    return entity;
  }
}
