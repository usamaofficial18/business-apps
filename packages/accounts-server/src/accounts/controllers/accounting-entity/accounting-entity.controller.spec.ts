import {
  TenantUserService,
  TokenCacheService,
  UserService,
} from '@castlecraft/auth';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { Test, TestingModule } from '@nestjs/testing';
import { AccountingEntityController } from './accounting-entity.controller';

describe('AccountingEntityController', () => {
  let controller: AccountingEntityController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AccountingEntityController],
      providers: [
        { provide: QueryBus, useValue: {} },
        { provide: CommandBus, useValue: {} },
        { provide: TokenCacheService, useValue: {} },
        { provide: UserService, useValue: {} },
        { provide: TenantUserService, useValue: {} },
      ],
    }).compile();

    controller = module.get<AccountingEntityController>(
      AccountingEntityController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
