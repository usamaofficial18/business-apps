import { IsUUID } from 'class-validator';

export class FetchAccountingEntityDto {
  @IsUUID()
  uuid: string;
}
