import { TenantUser } from '@castlecraft/auth';
import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  Req,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { AddJournalEntryCommand } from '../../commands/journal-entry/add-journal-entry/add-journal-entry.command';
import { RemoveJournalEntryCommand } from '../../commands/journal-entry/remove-journal-entry/remove-journal-entry.command';
import { UpdateJournalEntryCommand } from '../../commands/journal-entry/update-journal-entry/update-journal-entry.command';
import { JournalEntryDto } from '../../entities/journal-entry/journal-entry.dto';
import { GetJournalEntryQuery } from '../../queries/journal-entry/get-journal-entry/get-journal-entry.query';
import { ListJournalEntryQuery } from '../../queries/journal-entry/list-journal-entry/list-journal-entry.query';
import { ListJournalEntryDto } from './list-Journal-entry.dto';

@Controller('journal_entry')
export class JournalEntryController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async createAccountingEntity(
    @Body() journalEntryPayload: JournalEntryDto,
    @Req() req: unknown & { tenantUser: TenantUser },
  ) {
    return await this.commandBus.execute(
      new AddJournalEntryCommand(journalEntryPayload, req.tenantUser),
    );
  }

  @Get('v1/list')
  @UsePipes(new ValidationPipe())
  async getAccountingEntityList(@Query() query: ListJournalEntryDto) {
    return await this.queryBus.execute(new ListJournalEntryQuery(query));
  }

  @Get('v1/fetch/:uuid')
  @UsePipes(new ValidationPipe())
  async getAccountingEntity(@Param('uuid') uuid: string) {
    return await this.queryBus.execute(new GetJournalEntryQuery(uuid));
  }

  @Post('v1/remove/:uuid')
  async removeAccountingEntity(@Param('uuid') uuid) {
    return await this.commandBus.execute(new RemoveJournalEntryCommand(uuid));
  }

  @Post('v1/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async updateAccountingEntity(
    @Body() updatePayload: JournalEntryDto,
    @Req() req,
  ) {
    return await this.commandBus.execute(
      new UpdateJournalEntryCommand(updatePayload),
    );
  }
}
