import * as mongoose from 'mongoose';
import * as mongooseIntl from 'mongoose-intl';
import { v4 as uuidv4 } from 'uuid';

const schema = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4, index: true },
    tenantId: { type: String, index: true },
    accountingEntity: { type: String, index: true },
    currency: { type: String, index: true },
    parentAccount: { type: String, index: true },
    parentAccountNumber: { type: String, index: true },
    accountName: { type: String, index: true, intl: true },
    accountNumber: { type: String, index: true },
    level: Number,
    balanceType: String,
    classification: String,
  },
  { collection: 'accounts', versionKey: false },
);

export const Account = schema.plugin(mongooseIntl, {
  languages: ['en', 'ar'],
  defaultLanguage: 'en',
});

export const ACCOUNT = 'Account';
