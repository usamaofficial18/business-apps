import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { parseRegEx, stringEscape } from '../../../common/utils';
import { ListAccountDto } from '../../controllers/account/list-account.dto';
import { Account } from './account.interface';
import { ACCOUNT } from './account.schema';

@Injectable()
export class AccountService {
  constructor(
    @Inject(ACCOUNT)
    public readonly model: Model<Account>,
  ) {}

  async create(account: Account) {
    return await this.model.create(account);
  }

  async list(query: ListAccountDto) {
    const { limit, offset, sort, ...filterQuery } = query;

    Object.keys(filterQuery).forEach(key => {
      if (filterQuery[key] === undefined) {
        delete filterQuery[key];
      } else {
        try {
          filterQuery[key] = new RegExp(stringEscape(filterQuery[key]), 'i');
        } catch (error) {}
      }
    });
    const compoundQuery = {
      ...filterQuery,
    };
    const sortOrder = sort
      ? { [sort.split(' ')[0]]: sort.split(' ')[1] === 'asc' ? 1 : -1 }
      : { accountNumber: 1 };
    const results = await this.model
      .aggregate([
        { $match: compoundQuery },
        {
          $addFields: {
            accountNameEn: '$accountName.en',
            accountNameAr: '$accountName.ar',
          },
        },
        {
          $sort: sortOrder,
        },
      ])
      .skip(Number(offset || 0))
      .collation({ locale: 'en_US', numericOrdering: true })
      .limit(Number(limit || 10));

    return {
      docs: results || [],
      length: await this.model.countDocuments(compoundQuery),
      offset,
    };
  }

  async findOne(query) {
    return await this.model.findOne(query);
  }

  async deleteOne(query) {
    return await this.model.deleteOne(query);
  }

  async updateOne(query, param) {
    return await this.model.updateOne(query, param);
  }

  async loadRoot(query, params?) {
    return await this.model
      .find(query, params)
      .sort({ accountNumber: 1 })
      .collation({ locale: 'en_US', numericOrdering: true });
  }

  async loadChildren(parentAccount: string, params?) {
    return await this.model
      .find({ parentAccount }, params)
      .sort({ accountNumber: 1 })
      .collation({ locale: 'en_US', numericOrdering: true });
  }

  getFilterQuery(query) {
    const keys = Object.keys(query);
    keys.forEach(key => {
      if (typeof query[key] === 'string') {
        query[key] = { $regex: parseRegEx(query[key]), $options: 'i' };
      } else {
        delete query[key];
      }
      if (typeof query[key] !== 'undefined') {
      } else {
        delete query[key];
      }
    });
    return query;
  }

  async deleteMany(filter, options?) {
    return await this.model.deleteMany(filter, options);
  }
}
