import * as mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

const schema = new mongoose.Schema(
  {
    tenantId: { type: String, index: true },
    uuid: { type: String, default: uuidv4, index: true },
    name: { type: String, index: true },
    entityType: { type: String },
    taxId: { type: String },
    baseCurrency: { type: String },
    country: { type: String },
    abbr: { type: String },
    dateOfBirth: { type: Date },
    isImmutable: { type: Boolean, default: false },
  },
  { collection: 'accounting_entities', versionKey: false },
);

export const AccountingEntity = schema;

export const ACCOUNTING_ENTITY = 'Accounting Entity';
