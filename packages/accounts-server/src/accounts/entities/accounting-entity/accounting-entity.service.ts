import { Inject } from '@nestjs/common';
import { Injectable } from '@nestjs/common';
import { FilterQuery, Model, QueryOptions } from 'mongoose';
import { AccountingEntity } from './accounting-entity.interface';
import { ACCOUNTING_ENTITY } from './accounting-entity.schema';

@Injectable()
export class AccountingEntityService {
  constructor(
    @Inject(ACCOUNTING_ENTITY)
    public readonly model: Model<AccountingEntity>,
  ) {}

  async create(accountingEntity: AccountingEntity) {
    return await this.model.create(accountingEntity);
  }

  async findOne(query) {
    return await this.model.findOne(query);
  }

  async deleteOne(query) {
    return await this.model.deleteOne(query);
  }

  async updateOne(query, param) {
    return await this.model.updateOne(query, param);
  }

  async deleteMany(filter, options?) {
    return await this.model.deleteMany(filter, options);
  }

  async find(
    filter: FilterQuery<AccountingEntity>,
    projection?,
    options?: QueryOptions,
  ) {
    return await this.model.find(filter, projection, options);
  }
}
