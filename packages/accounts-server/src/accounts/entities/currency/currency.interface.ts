import { Document } from 'mongoose';

export interface Currency extends Document {
  name: string;
  code: string;
  minorUnit: number;
}
