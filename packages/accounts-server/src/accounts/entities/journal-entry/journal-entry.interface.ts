import { Document } from 'mongoose';

export interface JournalEntry extends Document {
  uuid: string;
  tenantId: string;
  date: Date;
  accountingEntity: string;
  accounts: JournalAccount[];
}

export interface JournalAccount {
  account: string;
  accountNameEn: string;
  debit: number;
  credit: number;
  debitInAccountCurrency: number;
  creditInAccountCurrency: number;
  transactionLink: string;
  remark: string;
}
