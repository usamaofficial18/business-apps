import { Test, TestingModule } from '@nestjs/testing';
import { LEDGER_ENTRY } from './ledger-entry.schema';
import { LedgerEntryService } from './ledger-entry.service';

describe('LedgerEntryService', () => {
  let service: LedgerEntryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        LedgerEntryService,
        {
          provide: LEDGER_ENTRY,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<LedgerEntryService>(LedgerEntryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
