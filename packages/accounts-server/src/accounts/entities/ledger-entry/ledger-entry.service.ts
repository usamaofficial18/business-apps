import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { LedgerEntry } from './ledger-entry.interface';
import { LEDGER_ENTRY } from './ledger-entry.schema';

@Injectable()
export class LedgerEntryService {
  constructor(
    @Inject(LEDGER_ENTRY)
    public readonly model: Model<LedgerEntry>,
  ) {}

  async create(ledgerEntry: LedgerEntry) {
    return await this.model.create(ledgerEntry);
  }

  async find(query) {
    return await this.model.find(query);
  }

  async findOne(query) {
    return await this.model.findOne(query);
  }

  async deleteOne(query) {
    return await this.model.deleteOne(query);
  }

  async insertMany(ledgerEntries) {
    return this.model.insertMany(ledgerEntries);
  }

  async deleteMany(query) {
    return await this.model.deleteMany(query);
  }
}
