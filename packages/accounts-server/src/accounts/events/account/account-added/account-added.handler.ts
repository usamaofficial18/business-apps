import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { AccountService } from '../../../entities/account/account.service';
import { AccountAddedEvent } from './account-added.event';

@EventsHandler(AccountAddedEvent)
export class AccountAddedHandler implements IEventHandler<AccountAddedEvent> {
  constructor(private readonly accountService: AccountService) {}
  async handle(event: AccountAddedEvent) {
    const { account } = event;
    await this.accountService.create(account);
  }
}
