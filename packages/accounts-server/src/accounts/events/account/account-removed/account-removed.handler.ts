import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { AccountService } from '../../../../accounts/entities/account/account.service';
import { AccountRemovedEvent } from './account-removed.event';

@EventsHandler(AccountRemovedEvent)
export class AccountRemovedHandler
  implements IEventHandler<AccountRemovedEvent>
{
  constructor(private readonly accountService: AccountService) {}
  async handle(event: AccountRemovedEvent) {
    const { account } = event;
    await this.accountService.deleteOne({ uuid: account.uuid });
  }
}
