import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { AccountService } from '../../entities/account/account.service';
import { AccountingEntityService } from '../../entities/accounting-entity/accounting-entity.service';
import { JournalEntryService } from '../../entities/journal-entry/journal-entry.service';
import { LedgerEntryService } from '../../entities/ledger-entry/ledger-entry.service';
import { AccountingEntityPurgedEvent } from './accounting-entity-purged.event';

@EventsHandler(AccountingEntityPurgedEvent)
export class AccountingEntityPurgedHandler
  implements IEventHandler<AccountingEntityPurgedEvent>
{
  constructor(
    private readonly accountingEntity: AccountingEntityService,
    private readonly account: AccountService,
    private readonly journalEntry: JournalEntryService,
    private readonly ledgerEntry: LedgerEntryService,
  ) {}

  handle(event: AccountingEntityPurgedEvent) {
    this.account
      .deleteMany({ accountingEntity: event.accountingEntity.uuid })
      .then(success => {})
      .catch(error => {});

    this.journalEntry
      .deleteMany({ accountingEntity: event.accountingEntity.uuid })
      .then(success => {})
      .catch(error => {});

    this.ledgerEntry
      .deleteMany({ accountingEntity: event.accountingEntity.uuid })
      .then(success => {})
      .catch(error => {});

    this.accountingEntity
      .deleteOne({ uuid: event.accountingEntity.uuid })
      .then(success => {})
      .catch(error => {});
  }
}
