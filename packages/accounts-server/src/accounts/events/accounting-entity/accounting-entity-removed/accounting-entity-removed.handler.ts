import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { AccountingEntityService } from '../../../entities/accounting-entity/accounting-entity.service';
import { AccountingEntityRemovedEvent } from './accounting-entity-removed.event';

@EventsHandler(AccountingEntityRemovedEvent)
export class AccountingEntityRemovedHandler
  implements IEventHandler<AccountingEntityRemovedEvent>
{
  constructor(
    private readonly accountingEntityService: AccountingEntityService,
  ) {}
  async handle(event: AccountingEntityRemovedEvent) {
    const { accountingEntity } = event;
    await this.accountingEntityService.deleteOne({
      uuid: accountingEntity.uuid,
    });
  }
}
