import { IEvent } from '@nestjs/cqrs';
import { AccountingEntityDto } from '../../../entities/accounting-entity/accounting-entity.dto';

export class AccountingEntityUpdatedEvent implements IEvent {
  constructor(public updatePayload: AccountingEntityDto) {}
}
