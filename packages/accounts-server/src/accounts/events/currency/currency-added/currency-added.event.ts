import { IEvent } from '@nestjs/cqrs';
import { Currency } from '../../../entities/currency/currency.interface';

export class CurrencyAddedEvent implements IEvent {
  constructor(public currency: Currency) {}
}
