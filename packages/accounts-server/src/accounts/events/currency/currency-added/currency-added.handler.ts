import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { CurrencyService } from '../../../../accounts/entities/currency/currency.service';
import { CurrencyAddedEvent } from './currency-added.event';

@EventsHandler(CurrencyAddedEvent)
export class CurrencyAddedHandler implements IEventHandler<CurrencyAddedEvent> {
  constructor(private readonly currencyService: CurrencyService) {}
  async handle(event: CurrencyAddedEvent) {
    const { currency } = event;
    await this.currencyService.create(currency);
  }
}
