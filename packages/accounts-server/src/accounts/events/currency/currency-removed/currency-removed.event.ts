import { IEvent } from '@nestjs/cqrs';
import { Currency } from '../../../../accounts/entities/currency/currency.interface';

export class CurrencyRemovedEvent implements IEvent {
  constructor(public currency: Currency) {}
}
