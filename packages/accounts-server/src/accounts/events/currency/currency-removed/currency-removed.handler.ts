import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { CurrencyService } from '../../../../accounts/entities/currency/currency.service';
import { CurrencyRemovedEvent } from './currency-removed.event';

@EventsHandler(CurrencyRemovedEvent)
export class CurrencyRemovedHandler
  implements IEventHandler<CurrencyRemovedEvent>
{
  constructor(private readonly currencyService: CurrencyService) {}
  async handle(event: CurrencyRemovedEvent) {
    const { currency } = event;
    await this.currencyService.deleteOne({ code: currency.code });
  }
}
