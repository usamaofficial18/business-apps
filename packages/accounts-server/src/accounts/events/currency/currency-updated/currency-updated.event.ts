import { IEvent } from '@nestjs/cqrs';
import { CurrencyDto } from '../../../../accounts/entities/currency/currency-dto';

export class CurrencyUpdatedEvent implements IEvent {
  constructor(public updatePayload: CurrencyDto) {}
}
