import { AccountAddedHandler } from './account/account-added/account-added.handler';
import { AccountRemovedHandler } from './account/account-removed/account-removed.handler';
import { AccountUpdatedHandler } from './account/account-updated/account-updated.handler';
import { AccountingEntityPurgedHandler } from './accounting-entity-purged/accounting-entity-purged.handler';
import { AccountingEntityAddedHandler } from './accounting-entity/accounting-entity-added/accounting-entity-added.handler';
import { AccountingEntityRemovedHandler } from './accounting-entity/accounting-entity-removed/accounting-entity-removed.handler';
import { AccountingEntityUpdatedHandler } from './accounting-entity/accounting-entity-updated/accounting-entity-updated.handler';
import { CurrencyAddedHandler } from './currency/currency-added/currency-added.handler';
import { CurrencyRemovedHandler } from './currency/currency-removed/currency-removed.handler';
import { CurrencyUpdatedHandler } from './currency/currency-updated/currency-updated.handler';
import { JournalEntryAddedHandler } from './journal-entry/journal-entry-added/journal-entry-added.handler';
import { JournalEntryRemovedHandler } from './journal-entry/journal-entry-removed/journal-entry-removed.handler';
import { JournalEntryUpdatedHandler } from './journal-entry/journal-entry-updated/journal-entry-updated.handler';
import { TenantEntitiesPurgedHandler } from './tenant-entities-purged/tenant-entities-purged.handler';

export const AccountEventManager = [
  AccountAddedHandler,
  AccountRemovedHandler,
  AccountUpdatedHandler,

  CurrencyAddedHandler,
  CurrencyRemovedHandler,
  CurrencyUpdatedHandler,

  AccountingEntityAddedHandler,
  AccountingEntityRemovedHandler,
  AccountingEntityUpdatedHandler,
  AccountingEntityPurgedHandler,

  JournalEntryAddedHandler,
  JournalEntryRemovedHandler,
  JournalEntryUpdatedHandler,

  TenantEntitiesPurgedHandler,
];
