import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { LedgerEntryService } from '../../../entities/ledger-entry/ledger-entry.service';
import { JournalEntryService } from '../../../entities/journal-entry/journal-entry.service';
import { JournalEntryAddedEvent } from './journal-entry-added.event';

@EventsHandler(JournalEntryAddedEvent)
export class JournalEntryAddedHandler
  implements IEventHandler<JournalEntryAddedEvent>
{
  constructor(
    private readonly journalEntryService: JournalEntryService,
    private readonly ledgerEntryService: LedgerEntryService,
  ) {}

  async handle(event: JournalEntryAddedEvent) {
    const { journalEntry, ledgerEntries } = event;
    await this.journalEntryService.create(journalEntry);
    ledgerEntries.forEach(entry => {
      entry.journalEntry = journalEntry.uuid;
    });
    await this.ledgerEntryService.insertMany(ledgerEntries);
  }
}
