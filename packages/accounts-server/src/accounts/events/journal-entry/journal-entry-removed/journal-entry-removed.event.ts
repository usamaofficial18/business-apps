import { IEvent } from '@nestjs/cqrs';
import { JournalEntry } from '../../../entities/journal-entry/journal-entry.interface';

export class JournalEntryRemovedEvent implements IEvent {
  constructor(public readonly journalEntry: JournalEntry) {}
}
