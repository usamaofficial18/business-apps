import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { LedgerEntryService } from '../../../entities/ledger-entry/ledger-entry.service';
import { JournalEntryService } from '../../../entities/journal-entry/journal-entry.service';
import { JournalEntryRemovedEvent } from './journal-entry-removed.event';

@EventsHandler(JournalEntryRemovedEvent)
export class JournalEntryRemovedHandler
  implements IEventHandler<JournalEntryRemovedEvent>
{
  constructor(
    private readonly journalEntryService: JournalEntryService,
    private readonly ledgerEntryService: LedgerEntryService,
  ) {}

  async handle(event: JournalEntryRemovedEvent) {
    const { journalEntry } = event;

    await this.ledgerEntryService.deleteMany({
      journalEntry: journalEntry.uuid,
    });

    await this.journalEntryService.deleteOne({
      uuid: journalEntry.uuid,
    });
  }
}
