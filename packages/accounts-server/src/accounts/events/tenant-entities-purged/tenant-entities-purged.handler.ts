import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { AccountService } from '../../entities/account/account.service';
import { AccountingEntityService } from '../../entities/accounting-entity/accounting-entity.service';
import { JournalEntryService } from '../../entities/journal-entry/journal-entry.service';
import { LedgerEntryService } from '../../entities/ledger-entry/ledger-entry.service';
import { TenantEntitiesPurgedEvent } from './tenant-entities-purged.event';

@EventsHandler(TenantEntitiesPurgedEvent)
export class TenantEntitiesPurgedHandler
  implements IEventHandler<TenantEntitiesPurgedEvent>
{
  constructor(
    private readonly accountingEntity: AccountingEntityService,
    private readonly account: AccountService,
    private readonly journalEntry: JournalEntryService,
    private readonly ledgerEntry: LedgerEntryService,
  ) {}

  handle(event: TenantEntitiesPurgedEvent) {
    this.accountingEntity.find({ tenantId: event.tenantId }).then(entities => {
      entities.forEach(entity => {
        this.account
          .deleteMany({ accountingEntity: entity?.uuid })
          .then(success => {})
          .catch(error => {});

        this.journalEntry
          .deleteMany({ accountingEntity: entity?.uuid })
          .then(success => {})
          .catch(error => {});

        this.ledgerEntry
          .deleteMany({ accountingEntity: entity?.uuid })
          .then(success => {})
          .catch(error => {});

        this.accountingEntity
          .deleteOne({ uuid: entity?.uuid })
          .then(success => {})
          .catch(error => {});
      });
    });
  }
}
