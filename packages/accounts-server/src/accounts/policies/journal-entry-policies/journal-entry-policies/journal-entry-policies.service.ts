import { Injectable, NotFoundException } from '@nestjs/common';
import { AccountService } from '../../../entities/account/account.service';
import { AccountingEntityService } from '../../../entities/accounting-entity/accounting-entity.service';
import { JournalEntryDto } from '../../../entities/journal-entry/journal-entry.dto';

@Injectable()
export class JournalEntryPoliciesService {
  constructor(
    private readonly accountService: AccountService,
    private readonly accountingEntityService: AccountingEntityService,
  ) {}

  async validateCurrency(payload: JournalEntryDto) {
    const accountingEntity = await this.accountingEntityService.findOne({
      uuid: payload.accountingEntity,
    });
    payload.accounts.forEach(async element => {
      const account = await this.accountService.findOne({
        uuid: element.account,
      });
      if (accountingEntity.baseCurrency === account.currency) {
        element.creditInAccountCurrency = element.credit;
        element.debitInAccountCurrency = element.debit;
      } else if (accountingEntity.baseCurrency !== account.currency) {
        if (element.credit && !element.creditInAccountCurrency) {
          throw new NotFoundException(
            'CreditInAccountCurrency cannot be empty',
          );
        }
        if (element.debit && !element.debitInAccountCurrency) {
          throw new NotFoundException('DebitInAccountCurrency cannot be empty');
        }
      }
    });
    return payload;
  }
}
