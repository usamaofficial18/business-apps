import { IQuery } from '@nestjs/cqrs';
import { ListAccountDto } from '../../../controllers/account/list-account.dto';

export class RetrieveAccountListQuery implements IQuery {
  constructor(public readonly filter: ListAccountDto) {}
}
