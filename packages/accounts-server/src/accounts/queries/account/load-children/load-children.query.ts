import { IQuery } from '@nestjs/cqrs';

export class LoadChildrenQuery implements IQuery {
  constructor(public readonly uuid: string) {}
}
