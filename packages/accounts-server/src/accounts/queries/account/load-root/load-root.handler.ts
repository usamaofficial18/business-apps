import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { AccountsAggregateService } from '../../../aggregates/accounts-aggregate/accounts-aggregate.service';
import { LoadRootQuery } from './load-root.query';

@QueryHandler(LoadRootQuery)
export class LoadRootHandler implements IQueryHandler<LoadRootQuery> {
  constructor(private readonly manager: AccountsAggregateService) {}
  async execute(query: LoadRootQuery) {
    return await this.manager.loadRoot(query.accountingEntity);
  }
}
