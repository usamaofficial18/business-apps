import { IQuery } from '@nestjs/cqrs';

export class LoadRootQuery implements IQuery {
  constructor(public readonly accountingEntity: string) {}
}
