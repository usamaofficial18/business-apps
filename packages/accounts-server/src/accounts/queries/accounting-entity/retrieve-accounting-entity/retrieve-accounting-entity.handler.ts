import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { AccountingEntityAggregateService } from '../../../aggregates/accounting-entity-aggregate/accounting-entity-aggregate.service';
import { RetrieveAccountingEntityQuery } from './retrieve-accounting-entity.query';

@QueryHandler(RetrieveAccountingEntityQuery)
export class RetrieveAccountingEntityHandler
  implements IQueryHandler<RetrieveAccountingEntityQuery>
{
  constructor(private readonly entity: AccountingEntityAggregateService) {}

  async execute(query: RetrieveAccountingEntityQuery) {
    return await this.entity.fetchEntity(query.uuid);
  }
}
