import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { CurrencyAggregateService } from '../../../aggregates/currency-aggregate/currency-aggregate.service';
import { RetrieveCurrencyListQuery } from './retrieve-currency-list.query';

@QueryHandler(RetrieveCurrencyListQuery)
export class RetrieveCurrencyListHandler
  implements IQueryHandler<RetrieveCurrencyListQuery>
{
  constructor(private readonly manager: CurrencyAggregateService) {}
  async execute(query: RetrieveCurrencyListQuery) {
    return await this.manager.getCurrencyList(query.currencyQuery);
  }
}
