import { IQuery } from '@nestjs/cqrs';
import { ListCurrencyDto } from '../../../controllers/currency/list-currency.dto';

export class RetrieveCurrencyListQuery implements IQuery {
  constructor(public readonly currencyQuery: ListCurrencyDto) {}
}
