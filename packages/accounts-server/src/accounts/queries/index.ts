import { RetrieveAccountListHandler } from './account/list-account/retrieve-account-list.handler';
import { LoadChildrenHandler } from './account/load-children/load-children.handler';
import { LoadRootHandler } from './account/load-root/load-root.handler';
import { RetrieveAccountingEntityListHandler } from './accounting-entity/retrieve-accounting-entity-list/retrieve-accounting-entity-list.handler';
import { RetrieveAccountingEntityHandler } from './accounting-entity/retrieve-accounting-entity/retrieve-accounting-entity.handler';
import { GetCurrencyHandler } from './currency/get-currency/get-currency.handler';
import { RetrieveCurrencyListHandler } from './currency/list-currency/retrieve-currency-list.handler';
import { GetJournalEntryHandler } from './journal-entry/get-journal-entry/get-journal-entry.handler';
import { ListJournalEntryHandler } from './journal-entry/list-journal-entry/list-journal-entry.handler';

export const AccountQueryManager = [
  RetrieveAccountListHandler,
  LoadRootHandler,
  LoadChildrenHandler,

  RetrieveCurrencyListHandler,
  GetCurrencyHandler,

  RetrieveAccountingEntityListHandler,
  RetrieveAccountingEntityHandler,

  GetJournalEntryHandler,
  ListJournalEntryHandler,
];
