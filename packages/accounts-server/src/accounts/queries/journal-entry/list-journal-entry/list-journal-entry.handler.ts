import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { JournalEntryAggregateService } from '../../../aggregates/journal-entry-aggregate/journal-entry-aggregate.service';
import { ListJournalEntryQuery } from './list-journal-entry.query';

@QueryHandler(ListJournalEntryQuery)
export class ListJournalEntryHandler
  implements IQueryHandler<ListJournalEntryQuery>
{
  constructor(private readonly manager: JournalEntryAggregateService) {}

  async execute(query: ListJournalEntryQuery) {
    return await this.manager.listJournalEntry(query.filter);
  }
}
