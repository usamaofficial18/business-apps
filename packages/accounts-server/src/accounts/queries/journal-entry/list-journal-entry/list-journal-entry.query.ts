import { IQuery } from '@nestjs/cqrs';
import { ListJournalEntryDto } from '../../../controllers/journal-entry/list-Journal-entry.dto';

export class ListJournalEntryQuery implements IQuery {
  constructor(public readonly filter: ListJournalEntryDto) {}
}
