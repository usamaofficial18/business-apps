import { NestFactory } from '@nestjs/core';
import { CurrencyAggregateService } from '../accounts/aggregates/currency-aggregate/currency-aggregate.service';
import { AppModule } from '../app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const currencies = app.get(CurrencyAggregateService);
  currencies.updateCurrenciesData().subscribe({
    next: success => {
      console.info(success); // eslint-disable-line
      process.exit(0);
    },
    error: console.error, // eslint-disable-line
  });
}
bootstrap();
