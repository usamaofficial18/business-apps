import { Observable } from 'rxjs';
import { share } from 'rxjs/operators';
import { INVALID_REGEX } from '../constants/app-strings';

export function fromStream(
  stream,
  finishEventName = 'end',
  dataEventName = 'data',
) {
  stream.pause();

  return new Observable(observer => {
    function dataHandler(data) {
      observer.next(data);
    }

    function errorHandler(err) {
      observer.error(err);
    }

    function endHandler() {
      observer.complete();
    }

    stream.addListener(dataEventName, dataHandler);
    stream.addListener('error', errorHandler);
    stream.addListener(finishEventName, endHandler);

    stream.resume();

    return () => {
      stream.removeListener(dataEventName, dataHandler);
      stream.removeListener('error', errorHandler);
      stream.removeListener(finishEventName, endHandler);
    };
  }).pipe(share());
}

export type TranslateStringAny = any;

export function stringEscape(s: string) {
  const hex = c => {
    const v = '0' + c.charCodeAt(0).toString(16);
    return '\\x' + v.substr(v.length - 2);
  };

  return s
    ? s
        .replace(/\\/g, '\\\\')
        .replace(/\n/g, '\\n')
        .replace(/\t/g, '\\t')
        .replace(/\v/g, '\\v')
        .replace(/'/g, "\\'")
        .replace(/"/g, '\\"')
        .replace(/[\x00-\x1F\x80-\x9F]/g, hex)
    : s;
}

export function parseRegEx(value: string) {
  return value
    .split('')
    .map(char => {
      return INVALID_REGEX.includes(char) ? `\\${char}` : char;
    })
    .join('');
}
