import { Logger, Provider } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { defer, Observable } from 'rxjs';
import { delay, retryWhen, scan } from 'rxjs/operators';
import {
  ConfigService,
  DB_HOST,
  DB_NAME,
  DB_PASSWORD,
  DB_USER,
  MONGO_URI_PREFIX,
} from './config/config.service';

export const DATABASE_CONNECTION = 'DATABASE_CONNECTION';
export const DATABASE_PROVIDER = 'DatabaseProvider';

export const DatabaseProvider: Provider = {
  provide: DATABASE_CONNECTION,
  useFactory: async (config: ConfigService) => {
    const mongoUriPrefix = config.get(MONGO_URI_PREFIX) || 'mongodb';
    const mongoOptions = 'retryWrites=true';
    return await defer(() => {
      return mongoose.createConnection(
        `${mongoUriPrefix}://${config.get(DB_USER)}:${config.get(
          DB_PASSWORD,
        )}@${config.get(DB_HOST).replace(/,\s*$/, '')}/${config.get(
          DB_NAME,
        )}?${mongoOptions}`,
        {
          useNewUrlParser: true,
          useUnifiedTopology: true,
          autoReconnect: false,
          reconnectTries: 0,
          reconnectInterval: 0,
          useCreateIndex: true,
        },
      );
    })
      .pipe(handleRetry())
      .toPromise();
  },
  inject: [ConfigService],
};

export function handleRetry(
  retryAttempts = 9,
  retryDelay = 3000,
): <T>(source: Observable<T>) => Observable<T> {
  return <T>(source: Observable<T>) =>
    source.pipe(
      retryWhen(e =>
        e.pipe(
          scan((errorCount, error) => {
            Logger.error(
              error,
              `Unable to connect to the database. Retrying (${
                errorCount + 1
              })...`,
              DATABASE_PROVIDER,
            );
            if (errorCount + 1 >= retryAttempts) {
              throw error;
            }
            return errorCount + 1;
          }, 0),
          delay(retryDelay),
        ),
      ),
    );
}
