import { AuthDbConnectionService } from '@castlecraft/auth';
import { HealthCheckError } from '@godaddy/terminus';
import { Injectable } from '@nestjs/common';
import { Transport } from '@nestjs/microservices';
import {
  HealthIndicator,
  HealthIndicatorFunction,
  HealthIndicatorResult,
  MicroserviceHealthIndicator,
} from '@nestjs/terminus';
import {
  ConfigService,
  EVENTS_HOST,
  EVENTS_PORT,
} from '../../../config/config.service';

export const HEALTHCHECK_STATUS = 'Auth Connection Failed';
export const HEALTHCHECK_TIMEOUT = 10000;
export const EVENTS_BUS_STATUS_NAME = 'event-bus';

@Injectable()
export class HealthCheckAggregateService extends HealthIndicator {
  constructor(
    private readonly connection: AuthDbConnectionService,
    private readonly microservice: MicroserviceHealthIndicator,
    private readonly config: ConfigService,
  ) {
    super();
  }

  createTerminusOptions(): HealthIndicatorFunction[] {
    const healthEndpoint = [() => this.isHealthy()];

    if (this.config.get(EVENTS_HOST) && this.config.get(EVENTS_PORT)) {
      healthEndpoint.push(async () =>
        this.microservice.pingCheck(EVENTS_BUS_STATUS_NAME, {
          transport: Transport.TCP,
          options: {
            host: this.config.get(EVENTS_HOST),
            port: Number(this.config.get(EVENTS_PORT)),
          },
          timeout: HEALTHCHECK_TIMEOUT,
        }),
      );
    }

    return healthEndpoint;
  }

  async isHealthy(): Promise<HealthIndicatorResult> {
    const isHealthy = this.connection.connection.readyState === 1;
    const result = this.getStatus('auth', isHealthy);

    if (isHealthy) {
      return result;
    }
    throw new HealthCheckError(HEALTHCHECK_STATUS, result);
  }
}
