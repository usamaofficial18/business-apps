import {
  TenantRoleService,
  TenantService,
  TenantUserService,
  UserService,
} from '@castlecraft/auth';
import { Test, TestingModule } from '@nestjs/testing';
import { IdentityService } from './identity.service';

describe('IdentityService', () => {
  let service: IdentityService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        IdentityService,
        { provide: TenantService, useValue: {} },
        { provide: TenantUserService, useValue: {} },
        { provide: TenantRoleService, useValue: {} },
        { provide: UserService, useValue: {} },
      ],
    }).compile();

    service = module.get<IdentityService>(IdentityService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
