import { Test, TestingModule } from '@nestjs/testing';
import { IdentityService } from '../identity/identity.service';
import { TenantAggregateService } from './tenant-aggregate.service';

describe('TenantAggregateService', () => {
  let service: TenantAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TenantAggregateService,
        { provide: IdentityService, useValue: {} },
      ],
    }).compile();

    service = module.get<TenantAggregateService>(TenantAggregateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
