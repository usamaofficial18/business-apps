import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { TenantAggregateService } from '../../aggregates/tenant-aggregate/tenant-aggregate.service';
import { CreateTenantRoleCommand } from './create-tenant-role.command';

@CommandHandler(CreateTenantRoleCommand)
export class CreateTenantRoleHandler
  implements ICommandHandler<CreateTenantRoleCommand>
{
  constructor(
    private readonly manager: TenantAggregateService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: CreateTenantRoleCommand) {
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const tenantRole = await aggregate.createTenantRole(
      command.actor,
      command.roleName,
    );
    aggregate.commit();
    return tenantRole;
  }
}
