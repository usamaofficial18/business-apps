import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { TenantAggregateService } from '../../aggregates/tenant-aggregate/tenant-aggregate.service';
import { DeleteTenantRoleCommand } from './delete-tenant-role.command';

@CommandHandler(DeleteTenantRoleCommand)
export class DeleteTenantRoleHandler
  implements ICommandHandler<DeleteTenantRoleCommand>
{
  constructor(
    private readonly manager: TenantAggregateService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: DeleteTenantRoleCommand) {
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const message = await aggregate.deleteTenantRole(
      command.actor,
      command.roleName,
    );
    aggregate.commit();
    return message;
  }
}
