import { TenantUser, User } from '@castlecraft/auth';
import { ICommand } from '@nestjs/cqrs';
import { TenantUserDto } from '../../controllers/tenant/tenant-user.dto';

export class EditTenantUserCommand implements ICommand {
  constructor(
    public readonly actor: User | TenantUser,
    public readonly payload: TenantUserDto,
  ) {}
}
