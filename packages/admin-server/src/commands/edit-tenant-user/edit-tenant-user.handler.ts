import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { TenantAggregateService } from '../../aggregates/tenant-aggregate/tenant-aggregate.service';
import { EditTenantUserCommand } from './edit-tenant-user.command';

@CommandHandler(EditTenantUserCommand)
export class EditTenantUserHandler
  implements ICommandHandler<EditTenantUserCommand>
{
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: TenantAggregateService,
  ) {}

  async execute(command: EditTenantUserCommand) {
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.updateTenantUser(command.actor, command.payload);
    aggregate.commit();
    return command.payload;
  }
}
