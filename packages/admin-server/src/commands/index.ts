import { AddTenantUserHandler } from './add-tenant-user/add-tenant-user.handler';
import { AddTenantHandler } from './add-tenant/add-tenant.handler';
import { CreateTenantRoleHandler } from './create-tenant-role/create-tenant-role.handler';
import { DeleteTenantRoleHandler } from './delete-tenant-role/delete-tenant-role.handler';
import { EditTenantUserHandler } from './edit-tenant-user/edit-tenant-user.handler';
import { RemoveTenantUserHandler } from './remove-tenant-user/remove-tenant-user.handler';
import { RemoveTenantHandler } from './remove-tenant/remove-tenant.handler';
import { UpdateTenantHandler } from './update-tenant/update-tenant.handler';

export const AdminCommandHandlers = [
  AddTenantUserHandler,
  AddTenantHandler,
  CreateTenantRoleHandler,
  DeleteTenantRoleHandler,
  RemoveTenantUserHandler,
  RemoveTenantHandler,
  EditTenantUserHandler,
  UpdateTenantHandler,
];
