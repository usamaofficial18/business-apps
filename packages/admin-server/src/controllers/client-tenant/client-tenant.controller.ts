import {
  Client,
  ClientGuard,
  TokenGuard,
  TrustedClientGuard,
} from '@castlecraft/auth';
import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { AddTenantUserCommand } from '../../commands/add-tenant-user/add-tenant-user.command';
import { AddTenantCommand } from '../../commands/add-tenant/add-tenant.command';
import { CreateTenantRoleCommand } from '../../commands/create-tenant-role/create-tenant-role.command';
import { DeleteTenantRoleCommand } from '../../commands/delete-tenant-role/delete-tenant-role.command';
import { EditTenantUserCommand } from '../../commands/edit-tenant-user/edit-tenant-user.command';
import { RemoveTenantUserCommand } from '../../commands/remove-tenant-user/remove-tenant-user.command';
import { RemoveTenantCommand } from '../../commands/remove-tenant/remove-tenant.command';
import { UpdateTenantCommand } from '../../commands/update-tenant/update-tenant.command';
import { FetchTenantUserQuery } from '../../queries/fetch-tenant-user/fetch-tenant-user.query';
import { FetchTenantQuery } from '../../queries/fetch-tenant/fetch-tenant.query';
import { ListTenantRoleQuery } from '../../queries/list-tenant-role/list-tenant-role.query';
import { ListTenantUserQuery } from '../../queries/list-tenant-user/list-tenant-user.query';
import { ListTenantQuery } from '../../queries/list-tenant/list-tenant.query';
import { ListRoleDto } from '../tenant/list-role.dto';
import { ListTenantUserDto } from '../tenant/list-tenant-user.dto';
import { ListTenantDto } from '../tenant/list-tenant.dto';
import { TenantRoleDto } from '../tenant/tenant-role.dto';
import { TenantUserDto } from '../tenant/tenant-user.dto';
import { TenantDto } from '../tenant/tenant.dto';

@Controller('client/tenant')
@UseGuards(TokenGuard, ClientGuard, TrustedClientGuard)
@UsePipes(new ValidationPipe())
export class ClientTenantController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/add')
  async addTenant(
    @Body() payload: TenantDto & { [key: string]: any },
    @Req() req: unknown & { tokenClient: Client },
  ) {
    return await this.commandBus.execute(
      new AddTenantCommand(req?.tokenClient, payload),
    );
  }

  @Post('v1/update')
  async updateTenant(
    @Body() payload: TenantDto & { [key: string]: any },
    @Req() req: unknown & { tokenClient: Client },
  ) {
    return await this.commandBus.execute(
      new UpdateTenantCommand(req?.tokenClient, payload),
    );
  }

  @Post('v1/remove/:uuid')
  async removeTenant(
    @Param('uuid') uuid: string,
    @Req() req: unknown & { tokenClient: Client },
  ) {
    return await this.commandBus.execute(
      new RemoveTenantCommand(req?.tokenClient, uuid),
    );
  }

  @Get('v1/fetch/:uuid')
  async getTenant(
    @Param('uuid') uuid: string,
    @Req() req: unknown & { tokenClient: Client },
  ) {
    return await this.queryBus.execute(new FetchTenantQuery(uuid));
  }

  @Get('v1/list')
  async listTenants(
    @Query() query: ListTenantDto,
    @Req() req: unknown & { tokenClient: Client },
  ) {
    return await this.queryBus.execute(
      new ListTenantQuery(req?.tokenClient, query),
    );
  }

  @Post('role/v1/add')
  async addRole(
    @Body() payload: TenantRoleDto,
    @Req() req: unknown & { tokenClient: Client },
  ) {
    return await this.commandBus.execute(
      new CreateTenantRoleCommand(req.tokenClient, payload.roleName),
    );
  }

  @Post('role/v1/remove/:roleName')
  async removeRole(
    @Param('roleName') roleName: string,
    @Req() req: unknown & { tokenClient: Client },
  ) {
    return await this.commandBus.execute(
      new DeleteTenantRoleCommand(req.tokenClient, roleName),
    );
  }

  @Get('role/v1/list')
  async listRole(
    @Query() query: ListRoleDto,
    @Req() req: unknown & { tokenClient: Client },
  ) {
    return await this.queryBus.execute(new ListTenantRoleQuery(query));
  }

  @Post('user/v1/add')
  async addTenantUser(
    @Body() payload: TenantUserDto,
    @Req() req: unknown & { tokenClient: Client },
  ) {
    return await this.commandBus.execute(
      new AddTenantUserCommand(req.tokenClient, payload),
    );
  }

  @Post('user/v1/update')
  async editTenantUser(
    @Body() payload: TenantUserDto,
    @Req() req: unknown & { tokenClient: Client },
  ) {
    return await this.commandBus.execute(
      new EditTenantUserCommand(req.tokenClient, payload),
    );
  }

  @Post('user/v1/remove')
  async removeTenantUser(
    @Body() payload: TenantUserDto,
    @Req() req: unknown & { tokenClient: Client },
  ) {
    return await this.commandBus.execute(
      new RemoveTenantUserCommand(req.tokenClient, payload),
    );
  }

  @Get('user/v1/fetch')
  async getTenantUser(
    @Query() query: Omit<TenantUserDto, 'roles'>,
    @Req() req: unknown & { tokenClient: Client },
  ) {
    return await this.queryBus.execute(new FetchTenantUserQuery(query));
  }

  @Get('user/v1/list')
  async listTenantUser(
    @Query() query: ListTenantUserDto,
    @Req() req: unknown & { tokenClient: Client },
  ) {
    return await this.queryBus.execute(
      new ListTenantUserQuery(req.tokenClient, query),
    );
  }
}
