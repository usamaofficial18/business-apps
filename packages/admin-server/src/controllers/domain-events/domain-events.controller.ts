import {
  ClientGuard,
  CreatedByActor,
  TokenGuard,
  UserGuard,
} from '@castlecraft/auth';
import {
  Controller,
  Get,
  Param,
  Query,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { QueryBus } from '@nestjs/cqrs';
import { FetchEventQuery } from '../../queries/fetch-event/fetch-event.query';
import { ListEventsQuery } from '../../queries/list-events/list-events.query';
import { ListEventDto } from './list-event.dto';

@Controller('domain_events')
export class DomainEventsController {
  constructor(private readonly queryBus: QueryBus) {}

  @Get('v1/list_for_client')
  @UseGuards(TokenGuard, ClientGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async listDomainEventsForClient(@Req() req, @Query() query: ListEventDto) {
    return await this.queryBus.execute(
      new ListEventsQuery(
        CreatedByActor.Client,
        req?.tokenClient?.clientId,
        query.eventId,
        query.eventName,
        query.eventFromService,
        new Date(query.eventFromDateTime),
        new Date(query.eventToDateTime),
        Number(query.offset),
        Number(query.limit),
      ),
    );
  }

  @Get('v1/list_for_client')
  @UseGuards(TokenGuard, UserGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async listDomainEventsForAdmin(@Req() req, @Query() query: ListEventDto) {
    return await this.queryBus.execute(
      new ListEventsQuery(
        CreatedByActor.User,
        req?.tokenClient?.clientId,
        query.eventId,
        query.eventName,
        query.eventFromService,
        new Date(query.eventFromDateTime),
        new Date(query.eventToDateTime),
        Number(query.offset),
        Number(query.limit),
      ),
    );
  }

  @Get('v1/fetch_for_admin/:uuid')
  async fetchDomainEventForAdmin(@Req() req, @Param('uuid') uuid: string) {
    return await this.queryBus.execute(
      new FetchEventQuery(uuid, CreatedByActor.User, req?.user?.uuid),
    );
  }

  @Get('v1/fetch_for_client/:uuid')
  async fetchDomainEventForClient(@Req() req, @Param('uuid') uuid: string) {
    return await this.queryBus.execute(
      new FetchEventQuery(
        uuid,
        CreatedByActor.Client,
        req?.tokenClient?.clientId,
      ),
    );
  }
}
