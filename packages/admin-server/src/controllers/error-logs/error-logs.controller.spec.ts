import {
  ClientService,
  TokenCacheService,
  UserService,
} from '@castlecraft/auth';
import { CqrsModule } from '@nestjs/cqrs';
import { Test, TestingModule } from '@nestjs/testing';
import { ErrorLogsController } from './error-logs.controller';

describe('ErrorLogsController', () => {
  let controller: ErrorLogsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      controllers: [ErrorLogsController],
      providers: [
        { provide: TokenCacheService, useValue: {} },
        { provide: UserService, useValue: {} },
        { provide: ClientService, useValue: {} },
      ],
    }).compile();

    controller = module.get<ErrorLogsController>(ErrorLogsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
