import { DomainEventsController } from './domain-events/domain-events.controller';
import { ErrorLogsController } from './error-logs/error-logs.controller';
import { AdminTenantController } from './admin-tenant/admin-tenant.controller';
import { ClientTenantController } from './client-tenant/client-tenant.controller';
import { UserTenantController } from './user-tenant/user-tenant.controller';

export const TenantControllers = [
  DomainEventsController,
  ErrorLogsController,
  AdminTenantController,
  ClientTenantController,
  UserTenantController,
];
