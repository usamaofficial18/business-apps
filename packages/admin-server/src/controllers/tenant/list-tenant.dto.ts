import { CreatedByActor } from '@castlecraft/auth';
import { IsNumberString, IsOptional, IsString } from 'class-validator';

export class ListTenantDto {
  @IsString()
  @IsOptional()
  uuid: string;
  @IsString()
  @IsOptional()
  createdById: string;
  @IsOptional()
  createdByActor: CreatedByActor;
  @IsNumberString()
  @IsOptional()
  offset: string;
  @IsNumberString()
  @IsOptional()
  limit: string;
  @IsOptional()
  sort: string;
}
