import { IsLowercase } from 'class-validator';

export class TenantRoleDto {
  @IsLowercase()
  roleName: string;
}
