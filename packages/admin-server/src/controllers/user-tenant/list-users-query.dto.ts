import { IsNumberString, IsOptional, IsString } from 'class-validator';

export class ListTokenUsersDto {
  @IsString()
  @IsOptional()
  uuid: string;
  @IsString()
  @IsOptional()
  email: string;
  @IsString()
  @IsOptional()
  phone: string;
  @IsString()
  @IsOptional()
  name: string;
  @IsNumberString()
  @IsOptional()
  offset: string;
  @IsNumberString()
  @IsOptional()
  limit: string;
  @IsString()
  @IsOptional()
  sort: string;
}
