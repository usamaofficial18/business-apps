import {
  TenantUserService,
  TokenCacheService,
  UserService,
} from '@castlecraft/auth';
import { CqrsModule } from '@nestjs/cqrs';
import { Test, TestingModule } from '@nestjs/testing';
import { UserTenantController } from './user-tenant.controller';

describe('UserTenantController', () => {
  let controller: UserTenantController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserTenantController],
      imports: [CqrsModule],
      providers: [
        { provide: TokenCacheService, useValue: {} },
        { provide: UserService, useValue: {} },
        { provide: TenantUserService, useValue: {} },
      ],
    }).compile();

    controller = module.get<UserTenantController>(UserTenantController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
