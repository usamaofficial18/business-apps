import {
  Roles,
  TenantGuard,
  TenantRoleGuard,
  TenantUser,
  TokenGuard,
  User,
  UserGuard,
} from '@castlecraft/auth';
import {
  Body,
  Controller,
  Get,
  Post,
  Query,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { AddTenantUserCommand } from '../../commands/add-tenant-user/add-tenant-user.command';
import { EditTenantUserCommand } from '../../commands/edit-tenant-user/edit-tenant-user.command';
import { RemoveTenantUserCommand } from '../../commands/remove-tenant-user/remove-tenant-user.command';
import { ADMINISTRATOR } from '../../constants/strings';
import { ListTenantForUsersQuery } from '../../queries/list-tenant-for-users/list-tenant-for-users.query';
import { ListTenantRoleQuery } from '../../queries/list-tenant-role/list-tenant-role.query';
import { ListTenantUserQuery } from '../../queries/list-tenant-user/list-tenant-user.query';
import { ListTokenUsersQuery } from '../../queries/list-token-users/list-token-users.query';
import { ListRoleDto } from '../tenant/list-role.dto';
import { ListTenantUserDto } from '../tenant/list-tenant-user.dto';
import { ListTenantDto } from '../tenant/list-tenant.dto';
import { TenantUserDto } from '../tenant/tenant-user.dto';
import { ListTokenUsersDto } from './list-users-query.dto';

@Controller('user/tenant')
export class UserTenantController {
  constructor(
    private readonly queryBus: QueryBus,
    private readonly commandBus: CommandBus,
  ) {}

  @Get('v1/list')
  @UseGuards(TokenGuard, UserGuard)
  async listTenant(
    @Req() req: unknown & { user: User; tenantUser: TenantUser },
    @Query() query: ListTenantDto,
  ) {
    return await this.queryBus.execute(
      new ListTenantForUsersQuery(req.user, query),
    );
  }

  @Get('v1/profile')
  @UseGuards(TokenGuard, UserGuard, TenantGuard)
  getProfile(@Req() req: unknown & { tenantUser: TenantUser; user: User }) {
    return {
      ...req.user,
      tenantRoles: req.tenantUser.roles,
    };
  }

  @Get('token_user/v1/list')
  @UseGuards(TokenGuard, UserGuard)
  async listTokenUsers(@Query() query: ListTokenUsersDto) {
    return await this.queryBus.execute(new ListTokenUsersQuery(query));
  }

  @Post('user/v1/add')
  @Roles(ADMINISTRATOR)
  @UseGuards(
    TokenGuard,
    UserGuard,
    TenantGuard,
    new TenantRoleGuard(new Reflector()),
  )
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async addTenantUser(
    @Req() req: unknown & { user: User },
    @Body() payload: TenantUserDto,
  ) {
    return await this.commandBus.execute(
      new AddTenantUserCommand(req.user, payload),
    );
  }

  @Get('role/v1/list')
  @UseGuards(TokenGuard, UserGuard)
  async listRole(
    @Query() query: ListRoleDto,
    @Req() req: unknown & { tenantUser: TenantUser },
  ) {
    return await this.queryBus.execute(new ListTenantRoleQuery(query));
  }

  @Get('user/v1/list')
  @UseGuards(TokenGuard, UserGuard, TenantGuard)
  async listTenantUser(
    @Query() query: ListTenantUserDto,
    @Req() req: unknown & { user: User; tenantUser: TenantUser },
  ) {
    return await this.queryBus.execute(
      new ListTenantUserQuery(req.user, query, req.tenantUser?.tenant),
    );
  }

  @Post('user/v1/remove')
  @Roles(ADMINISTRATOR)
  @UseGuards(
    TokenGuard,
    UserGuard,
    TenantGuard,
    new TenantRoleGuard(new Reflector()),
  )
  async removeTenantUser(
    @Body() payload: TenantUserDto,
    @Req() req: unknown & { tenantUser: TenantUser },
  ) {
    return await this.commandBus.execute(
      new RemoveTenantUserCommand(req.tenantUser, payload),
    );
  }

  @Post('user/v1/update')
  @Roles(ADMINISTRATOR)
  @UseGuards(
    TokenGuard,
    UserGuard,
    TenantGuard,
    new TenantRoleGuard(new Reflector()),
  )
  async editTenantUser(
    @Body() payload: TenantUserDto,
    @Req() req: unknown & { tenantUser: TenantUser },
  ) {
    return await this.commandBus.execute(
      new EditTenantUserCommand(req.tenantUser, payload),
    );
  }
}
