import { MqttOptions, Transport } from '@nestjs/microservices';
import {
  ConfigService,
  EVENTS_HOST,
  EVENTS_PASSWORD,
  EVENTS_PORT,
  EVENTS_PROTO,
  EVENTS_USER,
} from './config/config.service';

export const eventsConnectionFactory = (config: ConfigService): MqttOptions => {
  const url = `${config.get(EVENTS_PROTO)}://${config.get(
    EVENTS_USER,
  )}:${config.get(EVENTS_PASSWORD)}@${config.get(EVENTS_HOST)}:${config.get(
    EVENTS_PORT,
  )}`;

  return {
    transport: Transport.MQTT,
    options: { url, protocolVersion: 5 },
  };
};

export const RETRY_ATTEMPTS = 3;
export const RETRY_DELAY = 10;
