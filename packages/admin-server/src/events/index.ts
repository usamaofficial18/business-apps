import { TenantAddedHandler } from './tenant-added/tenant-added.handler';
import { TenantRemovedHandler } from './tenant-removed/tenant-removed.handler';
import { TenantRoleCreatedHandler } from './tenant-role-created/tenant-role-created.handler';
import { TenantRoleDeletedHandler } from './tenant-role-deleted/tenant-role-deleted.handler';
import { TenantUpdatedHandler } from './tenant-updated/tenant-updated.handler';
import { TenantUserAddedHandler } from './tenant-user-added/tenant-user-added.handler';
import { TenantUserRemovedHandler } from './tenant-user-removed/tenant-user-removed.handler';
import { TenantUserUpdatedHandler } from './tenant-user-updated/tenant-user-updated.handler';

export const AdminEventHandlers = [
  TenantAddedHandler,
  TenantUpdatedHandler,
  TenantRemovedHandler,
  TenantRoleCreatedHandler,
  TenantRoleDeletedHandler,
  TenantUserAddedHandler,
  TenantUserRemovedHandler,
  TenantUserUpdatedHandler,
];
