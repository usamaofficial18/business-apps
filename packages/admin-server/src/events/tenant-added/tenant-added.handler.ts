import { TenantService } from '@castlecraft/auth';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { TenantAddedEvent } from './tenant-added.event';

@EventsHandler(TenantAddedEvent)
export class TenantAddedHandler implements IEventHandler<TenantAddedEvent> {
  constructor(private readonly tenant: TenantService) {}
  handle(event: TenantAddedEvent) {
    if (event?.tenant) {
      this.tenant
        .insertOne(event.tenant)
        .then(added => {})
        .catch(err => {});
    }
  }
}
