import { Client, Tenant, User } from '@castlecraft/auth';
import { IEvent } from '@nestjs/cqrs';

export class TenantRemovedEvent implements IEvent {
  constructor(
    public readonly tenant: Tenant,
    public readonly actor: User | Client,
  ) {}
}
