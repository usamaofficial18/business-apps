import { Client, TenantRole, User } from '@castlecraft/auth';
import { IEvent } from '@nestjs/cqrs';

export class TenantRoleCreatedEvent implements IEvent {
  constructor(
    public readonly tenantRole: TenantRole,
    public readonly actor: User | Client,
  ) {}
}
