import { Client, User } from '@castlecraft/auth';
import { IEvent } from '@nestjs/cqrs';

export class TenantUpdatedEvent implements IEvent {
  constructor(
    public readonly actor: User | Client,
    public readonly payload: unknown & { uuid?: string },
  ) {}
}
