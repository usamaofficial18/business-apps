import { TenantService } from '@castlecraft/auth';
import { Logger } from '@nestjs/common';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { TenantUpdatedEvent } from './tenant-updated.event';

@EventsHandler(TenantUpdatedEvent)
export class TenantUpdatedHandler implements IEventHandler<TenantUpdatedEvent> {
  constructor(private readonly tenant: TenantService) {}

  handle(event: TenantUpdatedEvent) {
    const { uuid, ...payload } = event.payload;
    this.tenant
      .updateOne({ uuid }, { $set: payload })
      .then()
      .catch(error => Logger.error(error, error, this.constructor.name));
  }
}
