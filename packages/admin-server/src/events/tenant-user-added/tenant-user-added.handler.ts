import { TenantUserService } from '@castlecraft/auth';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { TenantUserAddedEvent } from './tenant-user-added.event';

@EventsHandler(TenantUserAddedEvent)
export class TenantUserAddedHandler
  implements IEventHandler<TenantUserAddedEvent>
{
  constructor(private readonly tenantUser: TenantUserService) {}
  handle(event: TenantUserAddedEvent) {
    if (event?.tenantUser) {
      this.tenantUser
        .insertOne(event?.tenantUser)
        .then(added => {})
        .catch(err => {});
    }
  }
}
