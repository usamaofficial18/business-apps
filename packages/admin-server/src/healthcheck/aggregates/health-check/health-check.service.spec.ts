import { AuthDbConnectionService } from '@castlecraft/auth';
import { EventsDbConnectionService } from '@castlecraft/event-log';
import { MicroserviceHealthIndicator } from '@nestjs/terminus';
import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from '../../../config/config.service';
import { HealthCheckAggregateService } from './health-check.service';

describe('HealthCheckAggregateService', () => {
  let service: HealthCheckAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        HealthCheckAggregateService,
        { provide: AuthDbConnectionService, useValue: {} },
        { provide: EventsDbConnectionService, useValue: {} },
        { provide: MicroserviceHealthIndicator, useValue: {} },
        { provide: ConfigService, useValue: {} },
      ],
    }).compile();

    service = module.get<HealthCheckAggregateService>(
      HealthCheckAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
