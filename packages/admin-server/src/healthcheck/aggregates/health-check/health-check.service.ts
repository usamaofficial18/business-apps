import { AuthDbConnectionService } from '@castlecraft/auth';
import { EventsDbConnectionService } from '@castlecraft/event-log';
import { HealthCheckError } from '@godaddy/terminus';
import { Injectable } from '@nestjs/common';
import { Transport } from '@nestjs/microservices';
import {
  HealthIndicator,
  HealthIndicatorFunction,
  HealthIndicatorResult,
  MicroserviceHealthIndicator,
} from '@nestjs/terminus';
import {
  ConfigService,
  EVENTS_HOST,
  EVENTS_PORT,
} from '../../../config/config.service';

export const AUTH_HEALTHCHECK_STATUS = 'Auth Connection Failed';
export const EVENTS_HEALTHCHECK_STATUS = 'Events Log Connection Failed';
export const AUTH_STATUS_NAME = 'auth';
export const EVENTS_LOG_STATUS_NAME = 'events-log';
export const HEALTHCHECK_TIMEOUT = 10000;
export const EVENTS_BUS_STATUS_NAME = 'event-bus';

@Injectable()
export class HealthCheckAggregateService extends HealthIndicator {
  constructor(
    private readonly auth: AuthDbConnectionService,
    private readonly events: EventsDbConnectionService,
    private readonly microservice: MicroserviceHealthIndicator,
    private readonly config: ConfigService,
  ) {
    super();
  }

  createTerminusOptions(): HealthIndicatorFunction[] {
    const healthEndpoint = [
      () => this.isAuthHealthy(),
      () => this.isEventsHealthy(),
    ];

    if (this.config.get(EVENTS_HOST) && this.config.get(EVENTS_PORT)) {
      healthEndpoint.push(async () =>
        this.microservice.pingCheck(EVENTS_BUS_STATUS_NAME, {
          transport: Transport.TCP,
          options: {
            host: this.config.get(EVENTS_HOST),
            port: Number(this.config.get(EVENTS_PORT)),
          },
          timeout: HEALTHCHECK_TIMEOUT,
        }),
      );
    }

    return healthEndpoint;
  }

  async isAuthHealthy(): Promise<HealthIndicatorResult> {
    const isHealthy = this.auth.connection.readyState === 1;
    const result = this.getStatus(AUTH_STATUS_NAME, isHealthy);

    if (isHealthy) {
      return result;
    }
    throw new HealthCheckError(AUTH_HEALTHCHECK_STATUS, result);
  }

  async isEventsHealthy(): Promise<HealthIndicatorResult> {
    const isHealthy = this.events.connection.readyState === 1;
    const result = this.getStatus(EVENTS_LOG_STATUS_NAME, isHealthy);

    if (isHealthy) {
      return result;
    }
    throw new HealthCheckError(EVENTS_HEALTHCHECK_STATUS, result);
  }
}
