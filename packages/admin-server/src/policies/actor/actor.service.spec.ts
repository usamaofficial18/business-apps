import {
  ClientService,
  TenantUserService,
  UserService,
} from '@castlecraft/auth';
import { Test, TestingModule } from '@nestjs/testing';
import { ActorService } from './actor.service';

describe('ActorService', () => {
  let service: ActorService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ActorService,
        { provide: ClientService, useValue: {} },
        { provide: UserService, useValue: {} },
        { provide: TenantUserService, useValue: {} },
      ],
    }).compile();

    service = module.get<ActorService>(ActorService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
