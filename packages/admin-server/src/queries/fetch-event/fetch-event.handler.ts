import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { EventAggregateService } from '../../aggregates/event-aggregate/event-aggregate.service';
import { FetchEventQuery } from './fetch-event.query';

@QueryHandler(FetchEventQuery)
export class FetchEventHandler implements IQueryHandler<FetchEventQuery> {
  constructor(private readonly event: EventAggregateService) {}

  async execute(query: FetchEventQuery) {
    return await this.event.fetchEvent(
      query.actorType,
      query.actorUuid,
      query.eventId,
    );
  }
}
