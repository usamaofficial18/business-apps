import { CreatedByActor } from '@castlecraft/auth';
import { IQuery } from '@nestjs/cqrs';

export class FetchEventQuery implements IQuery {
  constructor(
    public readonly eventId: string,
    public readonly actorType: CreatedByActor,
    public readonly actorUuid: string,
  ) {}
}
