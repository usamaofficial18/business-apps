import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { TenantAggregateService } from '../../aggregates/tenant-aggregate/tenant-aggregate.service';
import { FetchTenantUserQuery } from './fetch-tenant-user.query';

@QueryHandler(FetchTenantUserQuery)
export class FetchTenantUserHandler
  implements IQueryHandler<FetchTenantUserQuery>
{
  constructor(private readonly aggregate: TenantAggregateService) {}

  async execute(query: FetchTenantUserQuery) {
    return this.aggregate.fetchTenantUser(query.query, query.requestTenant);
  }
}
