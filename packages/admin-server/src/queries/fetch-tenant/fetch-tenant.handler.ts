import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { TenantAggregateService } from '../../aggregates/tenant-aggregate/tenant-aggregate.service';
import { FetchTenantQuery } from './fetch-tenant.query';

@QueryHandler(FetchTenantQuery)
export class FetchTenantHandler implements IQueryHandler<FetchTenantQuery> {
  constructor(private readonly aggregate: TenantAggregateService) {}

  async execute(query: FetchTenantQuery) {
    return await this.aggregate.fetchTenant(query.uuid, query.requestTenant);
  }
}
