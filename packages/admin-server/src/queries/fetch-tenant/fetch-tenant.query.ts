import { IQuery } from '@nestjs/cqrs';

export class FetchTenantQuery implements IQuery {
  constructor(
    public readonly uuid: string,
    public readonly requestTenant?: string,
  ) {}
}
