import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { ErrorLogAggregateService } from '../../aggregates/error-log-aggregate/error-log-aggregate.service';
import { ListErrorLogQuery } from './list-error-logs.query';

@QueryHandler(ListErrorLogQuery)
export class ListErrorLogHandler implements IQueryHandler<ListErrorLogQuery> {
  constructor(private readonly manager: ErrorLogAggregateService) {}
  async execute(query: ListErrorLogQuery) {
    return await this.manager.list(
      query.actorType,
      query.actorUuid,
      query.uuid,
      query.message,
      query.fromService,
      query.fromDateTime,
      query.toDataTime,
      query.offset,
      query.limit,
    );
  }
}
