import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { TenantAggregateService } from '../../aggregates/tenant-aggregate/tenant-aggregate.service';
import { ListTenantForUsersQuery } from './list-tenant-for-users.query';

@QueryHandler(ListTenantForUsersQuery)
export class ListTenantForUserHandler
  implements IQueryHandler<ListTenantForUsersQuery>
{
  constructor(private readonly manager: TenantAggregateService) {}
  async execute(query: ListTenantForUsersQuery) {
    return await this.manager.listTenantForUsers(query.actor, query.query);
  }
}
