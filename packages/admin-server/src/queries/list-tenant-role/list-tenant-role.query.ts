import { IQuery } from '@nestjs/cqrs';
import { ListRoleDto } from '../../controllers/tenant/list-role.dto';

export class ListTenantRoleQuery implements IQuery {
  constructor(public readonly query: ListRoleDto) {}
}
