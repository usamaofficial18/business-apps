import { Client, User } from '@castlecraft/auth';
import { IQuery } from '@nestjs/cqrs';
import { ListTenantUserDto } from '../../controllers/tenant/list-tenant-user.dto';

export class ListTenantUserQuery implements IQuery {
  constructor(
    public readonly actor: User | Client,
    public readonly query: ListTenantUserDto,
    public readonly requestTenant?: string,
  ) {}
}
