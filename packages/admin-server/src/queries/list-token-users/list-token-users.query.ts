import { IQuery } from '@nestjs/cqrs';
import { ListTokenUsersDto } from '../../controllers/user-tenant/list-users-query.dto';

export class ListTokenUsersQuery implements IQuery {
  constructor(public readonly filterQuery: ListTokenUsersDto) {}
}
