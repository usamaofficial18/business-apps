import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-account-form',
  templateUrl: './account-form.page.html',
  styleUrls: ['./account-form.page.scss'],
})
export class AccountFormPage implements OnInit {
  account = '';
  constructor(private readonly route: ActivatedRoute) {}

  ngOnInit() {
    this.account = this.route.snapshot.params.account;
  }
}
