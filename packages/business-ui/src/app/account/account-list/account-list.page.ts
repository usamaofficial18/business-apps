import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { debounceTime, map, startWith, switchMap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { LIST_ACCOUNTS, LIST_CURRENCIES } from '../../constants/url-strings';
import { DataListComponent } from '../../shared/components/data-list/data-list.component';
import { ListingService } from '../../shared/components/data-list/listing.service';

@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.page.html',
  styleUrls: ['./account-list.page.scss'],
})
export class AccountListPage implements OnInit {
  @ViewChild(DataListComponent)
  listComponent: DataListComponent;

  accountingEntity = '';
  linkCol = 'uuid';
  fields = [
    'accountNumber',
    'accountNameEn',
    'accountNameAr',
    'currency',
    'uuid',
  ];
  endpoint = environment.accountsServerUrl + LIST_ACCOUNTS;
  linkPrefix = '/account/';
  filters = {};
  account = new FormControl();
  parent = new FormControl();
  number = new FormControl();
  currency = new FormControl();
  accountNumbers: Observable<any>;
  parentNumbers: Observable<any>;
  accountNames: Observable<any>;
  currencies: Observable<any>;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly list: ListingService,
  ) {}

  ngOnInit() {
    this.accountingEntity = this.route.snapshot.params.accountingEntity;
    this.filters = { accountingEntity: this.accountingEntity };
    this.setAutoComplete();
  }

  setFilter() {
    this.filters = {
      ...this.filters,
      'accountName.en': this.account.value ? this.account.value : undefined,
      accountNumber: this.number.value ? this.number.value : undefined,
      parentAccountNumber: this.parent.value ? this.parent.value : undefined,
      currency: this.currency.value ? this.currency.value : undefined,
    };

    Object.keys(this.filters).forEach(key => {
      this.filters[key] === undefined && delete this.filters[key];
    });

    this.listComponent.dataSource.loadItems(this.filters);
  }

  clearFilter() {
    this.account.setValue('');
    this.parent.setValue('');
    this.number.setValue('');
    this.currency.setValue('');
    this.setFilter();
  }

  setAutoComplete() {
    this.accountNumbers = this.number.valueChanges.pipe(
      startWith(''),
      debounceTime(300),
      switchMap(val => this.filterAccountValueChange(val)),
      map(r => r.docs),
    );
    this.parentNumbers = this.parent.valueChanges.pipe(
      startWith(''),
      debounceTime(300),
      switchMap(val => this.filterAccountValueChange(val)),
      map(r => r.docs),
    );
    this.accountNames = this.account.valueChanges.pipe(
      startWith(''),
      debounceTime(300),
      switchMap(val => this.filterAccountValueChange(val)),
      map(r => r.docs),
    );
    this.currencies = this.currency.valueChanges.pipe(
      startWith(''),
      debounceTime(300),
      switchMap(val => this.filterCurrencyValueChange(val)),
      map(r => r.docs),
    );
  }

  filterAccountValueChange(val) {
    if (!isNaN(val)) {
      return this.list.findModels(this.endpoint, {
        accountingEntity: this.accountingEntity,
        accountNumber: val,
      });
    }
    return this.list.findModels(this.endpoint, {
      accountingEntity: this.accountingEntity,
      'accountName.en': val,
    });
  }

  filterCurrencyValueChange(code: string) {
    return this.list.findModels(
      environment.accountsServerUrl + LIST_CURRENCIES,
      { code },
    );
  }
}
