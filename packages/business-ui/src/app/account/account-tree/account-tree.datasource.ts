import { CollectionViewer, SelectionChange } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import {
  MatTreeFlatDataSource,
  MatTreeFlattener,
} from '@angular/material/tree';
import { BehaviorSubject, merge, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AccountNode } from '../../interfaces/account.node';
import { FlatNode } from '../../interfaces/flat.node';
import { AccountTreeService } from './account-tree.service';

export class AccountTreeDataSource extends MatTreeFlatDataSource<
  AccountNode,
  FlatNode
> {
  expandedNodes: FlatNode[] = [];

  dataChange = new BehaviorSubject<AccountNode[]>([]);

  get data(): AccountNode[] {
    return this.dataChange.value;
  }
  set data(value: AccountNode[]) {
    this.treeControl.dataNodes = this.treeFlattener.flattenNodes(value);
    this.dataChange.next(value);
  }

  constructor(
    private readonly treeControl: FlatTreeControl<FlatNode, FlatNode>,
    private readonly treeFlattener: MatTreeFlattener<AccountNode, FlatNode>,
    private readonly accountTreeService: AccountTreeService,
  ) {
    super(treeControl, treeFlattener);
  }

  connect(collectionViewer: CollectionViewer): Observable<FlatNode[]> {
    this.treeControl.expansionModel.changed.subscribe(change => {
      if (
        (change as SelectionChange<FlatNode>).added ||
        (change as SelectionChange<FlatNode>).removed
      ) {
        this.handleTreeControl(change as SelectionChange<FlatNode>);
      }
    });

    return merge(collectionViewer.viewChange, this.dataChange).pipe(
      map(() => this.data),
    );
  }

  disconnect() {}

  loadRoot(accountingEntity: string) {
    this.accountTreeService.getParent(accountingEntity).subscribe(res => {
      this.data = res
        .map(account => {
          return {
            ...account,
            expandable: account.children && account.children.length > 0,
          };
        })
        .sort((e1, e2) => e1.accountNumber - e2.accountNumber);
    });
  }

  /** Handle expand/collapse behaviors */
  handleTreeControl(change: SelectionChange<FlatNode>) {
    if (change.added) {
      change.added.forEach(node => this.toggleNode(node, true));
    }
    if (change.removed) {
      change.removed
        .slice()
        .reverse()
        .forEach(node => this.toggleNode(node, false));
    }
  }

  toggleNode(flatNode: FlatNode, expand: boolean) {
    flatNode.isLoading = true;
    this.accountTreeService.getChildren(flatNode.uuid).subscribe(data => {
      const index = this.data.indexOf(flatNode);
      if (!data || index < 0) {
        return;
      }

      if (expand) {
        const nodes = data
          .map(node => {
            return {
              tenantId: node.tenantId,
              accountingEntity: node.accountingEntity,
              currency: node.currency,
              parentAccount: node.parentAccount,
              accountNumber: node.accountNumber,
              parentAccountNumber: node.parentAccountNumber,
              accountName: node.accountName,
              level: node.level,
              expandable: node.children && node.children.length > 0,
              uuid: node.uuid,
              balanceType: node.balanceType,
              classification: node.classification,
            } as AccountNode;
          })
          .sort((e1, e2) => e1.accountNumber - e2.accountNumber);
        this.data.splice(index + 1, 0, ...nodes);
      } else {
        let count = 0;
        for (
          let i = index + 1;
          i < this.data.length && this.data[i].level > flatNode.level;
          i++, count++
        ) {}
        this.data.splice(index + 1, count);
      }
      // notify the change
      this.dataChange.next(this.data);
      flatNode.isLoading = false;
    });
  }
}
