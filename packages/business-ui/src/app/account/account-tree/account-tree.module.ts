import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { MaterialModule } from '../../material.module';
import { AccountTreePageRoutingModule } from './account-tree-routing.module';
import { AccountTreePage } from './account-tree.page';

const routes: Routes = [
  {
    path: '',
    component: AccountTreePage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    AccountTreePageRoutingModule,
    MaterialModule,
  ],
  declarations: [AccountTreePage],
})
export class AccountTreePageModule {}
