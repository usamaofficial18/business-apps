import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from '../../material.module';
import { AccountTreePage } from './account-tree.page';

describe('AccountTreePage', () => {
  let component: AccountTreePage;
  let fixture: ComponentFixture<AccountTreePage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [MaterialModule, HttpClientTestingModule, RouterTestingModule],
        declarations: [AccountTreePage],
      }).compileComponents();

      fixture = TestBed.createComponent(AccountTreePage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
