import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { AccountTreeService } from './account-tree.service';

describe('AccountTreeService', () => {
  let service: AccountTreeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(AccountTreeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
