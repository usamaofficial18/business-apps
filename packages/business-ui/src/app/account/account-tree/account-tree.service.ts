import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { LOAD_CHILDREN, LOAD_ROOT } from '../../constants/url-strings';

@Injectable({
  providedIn: 'root',
})
export class AccountTreeService {
  accountingEntity: string = '';

  constructor(private readonly http: HttpClient) {}

  getParent(accountingEntity: string) {
    return this.http.get<any>(
      environment.accountsServerUrl + LOAD_ROOT + '/' + accountingEntity,
    );
  }

  getChildren(parentAccount: string) {
    return this.http.get<any>(
      environment.accountsServerUrl + LOAD_CHILDREN + '/' + parentAccount,
    );
  }
}
