import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccountingEntityPage } from './accounting-entity.page';

const routes: Routes = [
  {
    path: '',
    component: AccountingEntityPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountingEntityPageRoutingModule {}
