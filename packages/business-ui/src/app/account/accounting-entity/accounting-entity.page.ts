import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription, throwError } from 'rxjs';
import {
  catchError,
  debounceTime,
  filter,
  map,
  startWith,
  switchMap,
} from 'rxjs/operators';
import { ConfirmationDialogComponent } from '../../shared/components/confirmation-dialog/confirmation-dialog.component';
import { environment } from '../../../environments/environment';
import { SET_ITEM, StorageService } from '../../auth/storage/storage.service';
import {
  DEFAULT_ACCOUNTING_ENTITY,
  SELECTED_TENANT,
} from '../../auth/token/constants';
import {
  SOMETHING_WENT_WRONG,
  UPPERCASE_DELETE,
  CONFIRM_DELETE_ENTITY,
  TYPE_DELETE_TO_PROCEED,
} from '../../constants/messages';
import {
  CREATE_ACCOUNTING_ENTITY,
  GET_ACCOUNTING_ENTITY,
  LIST_ACCOUNTING_ENTITIES,
  PURGE_ACCOUNTING_ENTITY,
} from '../../constants/url-strings';
import { ListingService } from '../../shared/components/data-list/listing.service';
import { AddEntityComponent } from './add-entity/add-entity.component';
import { UNDO_DURATION, X_TENANT_ID } from '../../constants/strings';

@Component({
  selector: 'app-accounting-entity',
  templateUrl: './accounting-entity.page.html',
  styleUrls: ['./accounting-entity.page.scss'],
})
export class AccountingEntityPage implements OnInit, OnDestroy {
  uuid: string;
  accountingEntity = new FormControl();
  accountingEntities: Observable<any>;
  form = new FormGroup({
    abbr: new FormControl(),
    baseCurrency: new FormControl(),
    country: new FormControl(),
    dateOfBirth: new FormControl(),
    entityType: new FormControl(),
    name: new FormControl(),
    taxId: new FormControl(),
    tenantId: new FormControl(),
  });
  entityName: string;
  dateOfBirth: string;
  tenantId: string;
  headers = {};
  storeValueChange: Subscription;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly list: ListingService,
    private readonly dialog: MatDialog,
    private readonly storage: StorageService,
    private readonly snackBar: MatSnackBar,
  ) {}

  ngOnDestroy(): void {
    this.storeValueChange && this.storeValueChange.unsubscribe();
  }

  ngOnInit() {
    this.setDefaultTenant().then(() => this.setupAutocomplete());
    this.uuid = this.route.snapshot.params.accountingEntity;
    if (this.uuid) {
      this.loadForm();
    }
    if (!this.uuid) {
      this.setDefaultEntity().then(() => this.setFilter());
    }
    this.storeValueChange = this.storage.changes.subscribe({
      next: res => {
        if (res.event === SET_ITEM && res.value?.key === SELECTED_TENANT) {
          const tenant = JSON.parse(res.value?.value || '{}');
          this.tenantId = tenant.tenantId;
          this.headers = { [X_TENANT_ID]: this.tenantId };
          this.setupAutocomplete();
          this.clearFilter();
        }
      },
      error: error => {},
    });
  }

  setupAutocomplete() {
    this.accountingEntities = this.accountingEntity.valueChanges.pipe(
      startWith(''),
      debounceTime(300),
      switchMap(val => {
        return this.list.findModels(
          environment.accountsServerUrl + LIST_ACCOUNTING_ENTITIES,
          { name: val },
          undefined,
          undefined,
          5,
          this.headers,
        );
      }),
      map(r => r.docs),
      catchError(error => {
        this.clearFilter();
        return throwError(error);
      }),
    );
  }

  setFilter() {
    this.uuid = this.accountingEntity.value?.uuid;
    const route = ['/accounting-entity'];
    if (this.uuid) {
      route.push(this.uuid);
      this.router.navigate(route).then(() => this.loadForm());
      return;
    }
    this.router.navigate(route);
    return;
  }

  clearFilter() {
    this.storage.removeItem(DEFAULT_ACCOUNTING_ENTITY);
    this.accountingEntity.setValue({});
    this.setFilter();
  }

  loadForm() {
    this.list
      .getRequest(
        environment.accountsServerUrl + GET_ACCOUNTING_ENTITY + '/' + this.uuid,
      )
      .subscribe({
        next: res => {
          this.entityName = res?.name;
          this.dateOfBirth = new Date(res?.dateOfBirth).toLocaleDateString();
          this.form.controls.abbr.setValue(res?.abbr);
          this.form.controls.baseCurrency.setValue(res?.baseCurrency);
          this.form.controls.baseCurrency.disable();
          this.form.controls.country.setValue(res?.country);
          this.form.controls.country.disable();
          this.form.controls.dateOfBirth.setValue(res?.dateOfBirth);
          this.form.controls.dateOfBirth.disable();
          this.form.controls.entityType.setValue(res?.entityType);
          this.form.controls.name.setValue(res?.name);
          this.form.controls.taxId.setValue(res?.taxId);
          const defaultEntity = { name: res?.name, uuid: res?.uuid };
          this.storage
            .setItem(DEFAULT_ACCOUNTING_ENTITY, JSON.stringify(defaultEntity))
            .then(() => this.setDefaultEntity());
        },
        error: error => {
          this.clearFilter();
        },
      });
  }

  openAddAccountingEntityDialog() {
    const dialogRef = this.dialog.open(AddEntityComponent, {
      width: '100vh',
      data: {},
    });

    dialogRef
      .afterClosed()
      .pipe(
        filter(res => res instanceof FormGroup),
        switchMap((form: FormGroup) => {
          return this.list.postRequest(
            environment.accountsServerUrl + CREATE_ACCOUNTING_ENTITY,
            {
              name: form.controls.name.value,
              entityType: form.controls.entityType.value,
              taxId: form.controls.taxId.value,
              baseCurrency: form.controls.baseCurrency.value,
              country: form.controls.country.value,
              abbr: form.controls.abbr.value,
              dateOfBirth: form.controls.dateOfBirth.value,
            },
            this.headers,
          );
        }),
      )
      .subscribe({
        next: res => {
          this.accountingEntity.setValue(res);
          this.setFilter();
        },
        error: error => {
          this.snackBar.open(
            error?.message || SOMETHING_WENT_WRONG,
            undefined,
            { duration: UNDO_DURATION },
          );
        },
      });
  }

  displayEntityName(entity?: { name: string; uuid: string }) {
    return entity?.name;
  }

  setDefaultEntity() {
    return this.storage.getItem(DEFAULT_ACCOUNTING_ENTITY).then(entity => {
      if (entity) {
        const defaultEntity = JSON.parse(entity);
        this.uuid = defaultEntity?.uuid;
        this.accountingEntity.setValue(defaultEntity);
      }
    });
  }

  setDefaultTenant() {
    return this.storage.getItem(SELECTED_TENANT).then(tenant => {
      if (tenant) {
        const defaultTenant = JSON.parse(tenant);
        this.tenantId = defaultTenant?.tenantId;
        this.headers = { [X_TENANT_ID]: this.tenantId };
      }
    });
  }

  confirmPurge() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent);
    dialogRef.componentInstance.confirmationInput = UPPERCASE_DELETE;
    dialogRef.componentInstance.message = TYPE_DELETE_TO_PROCEED;
    dialogRef.componentInstance.title = CONFIRM_DELETE_ENTITY;
    dialogRef.afterClosed().subscribe(isConfirm => {
      if (isConfirm) {
        this.purge();
      }
    });
  }

  purge() {
    this.list
      .postRequest(
        environment.accountsServerUrl +
          PURGE_ACCOUNTING_ENTITY +
          '/' +
          this.uuid,
      )
      .subscribe(() => this.clearFilter());
  }
}
