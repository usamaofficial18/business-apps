import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { startWith, debounceTime, switchMap, map } from 'rxjs/operators';
import { COUNTRIES } from '../../../constants/countries';
import { LIST_CURRENCIES } from '../../../constants/url-strings';
import { ListingService } from '../../../shared/components/data-list/listing.service';
import { environment } from '../../../../environments/environment';
import { StorageService } from '../../../auth/storage/storage.service';
import {
  DARK_MODE,
  DARK_MODE_CLASS_NAME,
  ENTITY_TYPE,
  ON,
} from '../../../constants/strings';

@Component({
  selector: 'app-add-entity',
  templateUrl: './add-entity.component.html',
  styleUrls: ['./add-entity.component.scss'],
})
export class AddEntityComponent implements OnInit {
  entityTypes: string[] = [];
  currencies: Observable<any>;
  countries: any = COUNTRIES;
  form = new FormGroup({
    name: new FormControl(),
    baseCurrency: new FormControl(),
    abbr: new FormControl(),
    taxId: new FormControl(),
    country: new FormControl(),
    entityType: new FormControl(),
    dateOfBirth: new FormControl(),
  });

  constructor(
    private readonly overlay: OverlayContainer,
    private readonly store: StorageService,
    private dialogRef: MatDialogRef<AddEntityComponent>,
    private readonly list: ListingService,
  ) {}

  ngOnInit() {
    this.form.controls.dateOfBirth.disable();
    this.store.getItem(DARK_MODE).then(darkMode => {
      if (darkMode === ON) {
        this.overlay.getContainerElement().classList.add(DARK_MODE_CLASS_NAME);
      } else {
        this.overlay
          .getContainerElement()
          .classList.remove(DARK_MODE_CLASS_NAME);
      }
    });
    this.entityTypes = Object.values(ENTITY_TYPE);

    this.currencies = this.form.controls.baseCurrency.valueChanges.pipe(
      startWith(''),
      debounceTime(300),
      switchMap(val => this.filterCurrencyValueChange(val)),
      map(r => r.docs),
    );
  }

  close(form?: FormGroup) {
    this.dialogRef.close(form);
  }

  filterCurrencyValueChange(code: string) {
    return this.list.findModels(
      environment.accountsServerUrl + LIST_CURRENCIES,
      { code },
    );
  }
}
