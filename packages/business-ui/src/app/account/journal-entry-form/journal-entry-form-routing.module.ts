import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JournalEntryFormPage } from './journal-entry-form.page';

const routes: Routes = [
  {
    path: '',
    component: JournalEntryFormPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JournalEntryFormPageRoutingModule {}
