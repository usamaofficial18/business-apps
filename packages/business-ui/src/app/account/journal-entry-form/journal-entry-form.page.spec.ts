import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RouterTestingModule } from '@angular/router/testing';

import { JournalEntryFormPage } from './journal-entry-form.page';

describe('JournalEntryFormPage', () => {
  let component: JournalEntryFormPage;
  let fixture: ComponentFixture<JournalEntryFormPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [JournalEntryFormPage],
        imports: [RouterTestingModule, HttpClientTestingModule],
        providers: [
          {
            provide: MatSnackBar,
            useValue: {},
          },
          {
            provide: MatDialog,
            useValue: {},
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(JournalEntryFormPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
