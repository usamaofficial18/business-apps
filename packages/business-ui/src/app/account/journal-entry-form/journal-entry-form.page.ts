import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { map, filter } from 'rxjs/operators';
import { StorageService } from '../../auth/storage/storage.service';

import {
  DEFAULT_ACCOUNTING_ENTITY,
  SELECTED_TENANT,
} from '../../auth/token/constants';
import {
  CANNOT_CREATE,
  CANNOT_DELETE,
  CANNOT_UPDATE,
  CONFIRM_DELETE_JOURNAL_ENTRY,
  JOURNAL_ENTRY,
  TYPE_DELETE_TO_PROCEED,
  UPDATED,
  UPPERCASE_DELETE,
} from '../../constants/messages';
import { CLOSE, UNDO_DURATION } from '../../constants/strings';
import {
  CREATE_JOURNAL_ENTRY,
  LIST_JOURNAL_ENTRIES,
  REMOVE_JOURNAL_ENTRY,
  UPDATE_JOURNAL_ENTRY,
} from '../../constants/url-strings';
import { ConfirmationDialogComponent } from '../../shared/components/confirmation-dialog/confirmation-dialog.component';
import { ListingService } from '../../shared/components/data-list/listing.service';
import { environment } from '../../../environments/environment';
import { JournalEntryAccountsDataSource } from '../journal-entry/journal-entry-accounts-datasource';
import {
  JournalEntry,
  JournalAccount,
} from '../journal-entry/journal.interface';
import { MatFabMenu } from '@angular-material-extensions/fab-menu';

@Component({
  selector: 'app-journal-entry-form',
  templateUrl: './journal-entry-form.page.html',
  styleUrls: ['./journal-entry-form.page.scss'],
})
export class JournalEntryFormPage implements OnInit {
  defaultTenant = '';
  defaultAccountingEntity = '';
  uuid: '';
  calledFrom: string;
  journalEntry: JournalEntry;
  dataSource: JournalEntryAccountsDataSource;
  displayedColumns = [
    'account',
    'debit',
    'credit',
    'debitInAccountCurrency',
    'creditInAccountCurrency',
    'transactionLink',
    'remark',
    'delete',
  ];
  journalEntryForm: FormGroup;
  accountControl: FormArray;
  actionButtons: MatFabMenu[] = [
    {
      id: 'save',
      icon: 'save',
      tooltip: 'Save',
      tooltipPosition: 'below',
    },
    {
      id: 'delete',
      icon: 'delete',
      tooltip: 'Delete',
      tooltipPosition: 'below',
    },
  ];

  get f() {
    return this.journalEntryForm.controls;
  }

  constructor(
    private readonly route: ActivatedRoute,
    private readonly service: ListingService,
    private readonly storage: StorageService,
    private readonly listingService: ListingService,
    private readonly snackBar: MatSnackBar,
    private readonly router: Router,
    private readonly dialog: MatDialog,
  ) {}

  ngOnInit() {
    this.createFormGroup();
    this.calledFrom = this.route.snapshot.params.calledFrom;
    this.dataSource = new JournalEntryAccountsDataSource();
    this.f.date.setValue(new Date());
    this.f.date.disable();

    if (this.calledFrom === 'edit') {
      this.uuid = this.route.snapshot.params.uuid;
      this.service
        .getRequest(environment.accountsServerUrl + LIST_JOURNAL_ENTRIES, {
          uuid: this.uuid,
          offset: 0,
          limit: 1,
        })
        .pipe(
          map(data => data.docs),
          filter(docs => docs.length),
          map(docs => docs[0]),
        )
        .subscribe(res => {
          this.f.date.setValue(new Date(res.date));
          this.dataSource.loadItems(res.accounts);
          res.accounts.forEach(account => {
            this.accountControl.push(new FormControl(account));
          });
        });
    }

    this.storage.getItem(DEFAULT_ACCOUNTING_ENTITY).then(entity => {
      if (entity) {
        this.defaultAccountingEntity = JSON.parse(entity).uuid;
      } else {
        this.actionButtons.splice(0, 1);
      }
    });
    this.storage.getItem(SELECTED_TENANT).then(tenant => {
      if (tenant) {
        this.defaultTenant = JSON.parse(tenant).tenantId;
      }
    });
  }

  createFormGroup() {
    this.journalEntryForm = new FormGroup({
      date: new FormControl('', [Validators.required]),
      accounts: new FormArray([], this.accountsValidator),
    });
    this.accountControl = this.journalEntryForm.get('accounts') as FormArray;
  }

  accountsValidator(accounts: FormArray) {
    if (accounts.length === 0) {
      return { accounts: true };
    } else {
      const accountsList = accounts
        .getRawValue()
        .filter(account => account.account !== '');
      if (accountsList.length !== accounts.length) {
        return { accounts: true };
      } else {
        return null;
      }
    }
  }

  addAccount() {
    const data = this.dataSource.data();
    const journalAccount = {} as JournalAccount;
    journalAccount.account = '';
    journalAccount.debit = 0;
    journalAccount.credit = 0;
    journalAccount.debitInAccountCurrency = 0;
    journalAccount.creditInAccountCurrency = 0;
    journalAccount.transactionLink = '';
    journalAccount.remark = '';

    data.push(journalAccount);
    this.accountControl.push(new FormControl(journalAccount));
    this.dataSource.update(data);
  }

  UpdateField(row: JournalAccount, index: number, account: JournalAccount) {
    if (account == null) {
      return;
    }
    Object.assign(row, account);
    if (account.account) {
      row.account = account.account;
    }
    if (account.debit) {
      row.debit = account.debit;
    }
    if (account.credit) {
      row.credit = account.credit;
    }
    if (account.debitInAccountCurrency) {
      row.debitInAccountCurrency = account.debitInAccountCurrency;
    }
    if (account.creditInAccountCurrency) {
      row.creditInAccountCurrency = account.creditInAccountCurrency;
    }
    if (account.transactionLink) {
      row.transactionLink = account.transactionLink;
    }
    if (account.remark) {
      row.remark = account.remark;
    }
    const copy = this.dataSource.data().slice();

    this.dataSource.update(copy);
    this.accountControl.controls[index].setValue(account);
  }

  deleteRow(i: number) {
    this.dataSource.data().splice(i, 1);
    this.accountControl.removeAt(i);
    this.dataSource.update(this.dataSource.data());
  }

  generatePayload() {
    const journalEntry = {} as JournalEntry;
    journalEntry.uuid = this?.uuid;
    journalEntry.date = new Date(this.journalEntryForm.controls.date.value);
    journalEntry.accountingEntity = this.defaultAccountingEntity;
    journalEntry.tenantId = this.defaultTenant;
    journalEntry.accounts = [] as JournalAccount[];
    const accounts = this.dataSource.data();
    let sumOfDebit = 0;
    let sumOfCredit = 0;
    let sumOfDebitInAccountCurrency = 0;
    let sumOfCreditInAccountCurrency = 0;
    accounts.forEach(data => {
      sumOfDebit = sumOfDebit + data.debit;
      sumOfCredit = sumOfCredit + data.credit;

      sumOfDebitInAccountCurrency =
        sumOfDebitInAccountCurrency + data.debitInAccountCurrency;
      sumOfCreditInAccountCurrency =
        sumOfCreditInAccountCurrency + data.creditInAccountCurrency;

      const account = {} as JournalAccount;
      account.account = data.account;
      account.accountNameEn = data.accountNameEn;
      account.credit = data.credit;
      account.debit = data.debit;
      account.creditInAccountCurrency = data.creditInAccountCurrency;
      account.debitInAccountCurrency = data.debitInAccountCurrency;
      account.transactionLink = data.transactionLink;
      account.remark = data.remark;
      journalEntry.accounts.push(account);
    });
    if (sumOfCredit !== sumOfDebit) {
      this.snackBar.open('Sum of Credit and Debit are not equal', CLOSE, {
        duration: 3000,
      });
    } else if (sumOfCreditInAccountCurrency !== sumOfDebitInAccountCurrency) {
      this.snackBar.open(
        'Sum of Credit and Debit in account currency are not equal',
        CLOSE,
        { duration: 3000 },
      );
    } else {
      return journalEntry;
    }
  }

  addJournalEntry() {
    const journalEntry = this.generatePayload();
    if (journalEntry.accounts.length) {
      const url = environment.accountsServerUrl + CREATE_JOURNAL_ENTRY;
      this.listingService.postRequest(url, journalEntry).subscribe({
        next: success => {
          this.router.navigate(['/journal-entry/edit', success.uuid]);
        },
      });
    } else {
      this.snackBar.open(CANNOT_CREATE, undefined, { duration: UNDO_DURATION });
    }
  }

  confirmPurge() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent);
    dialogRef.componentInstance.confirmationInput = UPPERCASE_DELETE;
    dialogRef.componentInstance.message = TYPE_DELETE_TO_PROCEED;
    dialogRef.componentInstance.title = CONFIRM_DELETE_JOURNAL_ENTRY;
    dialogRef.afterClosed().subscribe(isConfirm => {
      if (isConfirm) {
        this.deleteJournalEntry();
      }
    });
  }

  updateJournalEntry() {
    const journalEntry = this.generatePayload();
    this.listingService
      .postRequest(
        environment.accountsServerUrl + UPDATE_JOURNAL_ENTRY,
        journalEntry,
      )
      .subscribe({
        next: success => {
          this.snackBar.open(`${JOURNAL_ENTRY} ${UPDATED}`, CLOSE, {
            duration: UNDO_DURATION,
          });
        },
        error: error => {
          this.snackBar.open(`${CANNOT_UPDATE} ${JOURNAL_ENTRY}`, CLOSE, {
            duration: UNDO_DURATION,
          });
        },
      });
  }

  deleteJournalEntry() {
    this.listingService
      .postRequest(
        environment.accountsServerUrl + REMOVE_JOURNAL_ENTRY + '/' + this.uuid,
      )
      .subscribe({
        next: success => {
          this.router.navigate(['/journal-entry']);
        },
        error: error => {
          this.snackBar.open(`${CANNOT_DELETE} ${JOURNAL_ENTRY}`, CLOSE, {
            duration: UNDO_DURATION,
          });
        },
      });
  }

  selectedAction = event => {
    if (this.calledFrom === 'edit') {
      if (event === 'save') {
        this.updateJournalEntry();
      } else if (event === 'delete') {
        this.deleteJournalEntry();
      }
    }
  };
}
