import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JournalEntryPageRoutingModule } from './journal-entry-routing.module';
import { JournalEntryPage } from './journal-entry.page';
import { MaterialModule } from '../../material.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
    JournalEntryPageRoutingModule,
    MaterialModule,
  ],
  declarations: [JournalEntryPage],
  exports: [],
})
export class JournalEntryPageModule {}
