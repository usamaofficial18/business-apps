import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDialogRef } from '@angular/material/dialog';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';
import { MaterialModule } from '../../../material.module';
import { ListingService } from '../../../shared/components/data-list/listing.service';
import { AddTenantUserComponent } from './add-tenant-user.component';

describe('AddTenantUserComponent', () => {
  let component: AddTenantUserComponent;
  let fixture: ComponentFixture<AddTenantUserComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [AddTenantUserComponent],
        imports: [
          MaterialModule,
          HttpClientTestingModule,
          NoopAnimationsModule,
        ],
        providers: [
          {
            provide: MatDialogRef,
            useValue: {},
          },
          {
            provide: ListingService,
            useValue: {
              findModels: (...args) => of({ docs: [] }),
            } as ListingService,
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(AddTenantUserComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
