import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDialogRef } from '@angular/material/dialog';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { StorageService } from '../../../auth/storage/storage.service';
import { MaterialModule } from '../../../material.module';
import { ConfirmAddTenantComponent } from './confirm-add-tenant.component';

describe('ConfirmAddTenantComponent', () => {
  let component: ConfirmAddTenantComponent;
  let fixture: ComponentFixture<ConfirmAddTenantComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ConfirmAddTenantComponent],
        imports: [MaterialModule, NoopAnimationsModule],
        providers: [
          {
            provide: StorageService,
            useValue: {
              getItem: (...args) => Promise.resolve(''),
            } as StorageService,
          },
          {
            provide: MatDialogRef,
            useValue: {},
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(ConfirmAddTenantComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
