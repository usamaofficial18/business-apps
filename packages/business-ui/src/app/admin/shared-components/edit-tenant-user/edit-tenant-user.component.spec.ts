import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';
import { StorageService } from '../../../auth/storage/storage.service';
import { MaterialModule } from '../../../material.module';
import { ListingService } from '../../../shared/components/data-list/listing.service';
import { EditTenantUserComponent } from './edit-tenant-user.component';

describe('EditTenantUserComponent', () => {
  let component: EditTenantUserComponent;
  let fixture: ComponentFixture<EditTenantUserComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [EditTenantUserComponent],
        imports: [MaterialModule, NoopAnimationsModule],
        providers: [
          { provide: MatDialogRef, useValue: {} },
          { provide: MAT_DIALOG_DATA, useValue: {} },
          {
            provide: ListingService,
            useValue: {
              getRequest: (...args) => of({ docs: [] }),
            } as ListingService,
          },
          {
            provide: StorageService,
            useValue: {
              getItem: (...args) => Promise.resolve(''),
            } as StorageService,
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(EditTenantUserComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
