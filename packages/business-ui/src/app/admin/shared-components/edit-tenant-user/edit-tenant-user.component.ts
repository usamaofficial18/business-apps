import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { environment } from '../../../../environments/environment';
import { StorageService } from '../../../auth/storage/storage.service';
import {
  DARK_MODE,
  DARK_MODE_CLASS_NAME,
  ON,
} from '../../../constants/strings';
import { LIST_TENANT_ROLES_FOR_USER } from '../../../constants/url-strings';
import { ListingService } from '../../../shared/components/data-list/listing.service';

@Component({
  selector: 'app-edit-tenant-user',
  templateUrl: './edit-tenant-user.component.html',
  styleUrls: ['./edit-tenant-user.component.scss'],
})
export class EditTenantUserComponent implements OnInit {
  email = new FormControl();
  selectedRoles = new FormControl();
  roles = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private readonly overlay: OverlayContainer,
    private readonly store: StorageService,
    private readonly dialogRef: MatDialogRef<EditTenantUserComponent>,
    private readonly list: ListingService,
  ) {}

  ngOnInit() {
    this.store.getItem(DARK_MODE).then(darkMode => {
      if (darkMode === ON) {
        this.overlay.getContainerElement().classList.add(DARK_MODE_CLASS_NAME);
      } else {
        this.overlay
          .getContainerElement()
          .classList.remove(DARK_MODE_CLASS_NAME);
      }
    });
    this.email.disable();
    this.email.setValue(this.data.email);
    this.list
      .getRequest(environment.adminServerUrl + LIST_TENANT_ROLES_FOR_USER)
      .subscribe(res => {
        this.roles = res.docs;
        this.selectedRoles.setValue(this.data.roles);
      });
  }

  close() {
    this.dialogRef.close(false);
  }

  update() {
    this.dialogRef.close({ ...this.data, roles: this.selectedRoles.value });
  }

  delete() {
    this.dialogRef.close({ ...this.data, delete: true });
  }
}
