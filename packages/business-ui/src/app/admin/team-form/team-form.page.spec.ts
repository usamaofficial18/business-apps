import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { MaterialModule } from '../../material.module';
import { ListingService } from '../../shared/components/data-list/listing.service';
import { TeamFormPage } from './team-form.page';

describe('TeamFormPage', () => {
  let component: TeamFormPage;
  let fixture: ComponentFixture<TeamFormPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [TeamFormPage],
        imports: [RouterTestingModule, MaterialModule],
        providers: [
          {
            provide: ListingService,
            useValue: {
              postRequest: (...args) => of({}),
              getRequest: (...args) => of({ docs: [] }),
            } as ListingService,
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(TeamFormPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
