import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';
import { SharedModule } from '../../shared/shared.module';
import { TenantSharedComponentsModule } from '../shared-components/tenant-shared-components.module';
import { TenantFormPageRoutingModule } from './tenant-form-routing.module';
import { TenantFormPage } from './tenant-form.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    TenantFormPageRoutingModule,
    SharedModule,
    TenantSharedComponentsModule,
  ],
  declarations: [TenantFormPage],
})
export class TenantFormPageModule {}
