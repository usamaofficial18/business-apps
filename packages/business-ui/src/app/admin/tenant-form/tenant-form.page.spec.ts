import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { MaterialModule } from '../../material.module';
import { ListingService } from '../../shared/components/data-list/listing.service';
import { TenantFormPage } from './tenant-form.page';

describe('TenantFormPage', () => {
  let component: TenantFormPage;
  let fixture: ComponentFixture<TenantFormPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [TenantFormPage],
        imports: [RouterTestingModule, MaterialModule],
        providers: [
          {
            provide: ListingService,
            useValue: {
              postRequest: (...args) => of({}),
              getRequest: (...args) => of({ docs: [] }),
            } as ListingService,
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(TenantFormPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
