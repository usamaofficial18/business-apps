import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { MaterialModule } from '../../material.module';
import { ListingService } from '../../shared/components/data-list/listing.service';
import { TenantListPage } from './tenant-list.page';

describe('TenantListPage', () => {
  let component: TenantListPage;
  let fixture: ComponentFixture<TenantListPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [TenantListPage],
        imports: [
          HttpClientTestingModule,
          RouterTestingModule,
          MaterialModule,
          NoopAnimationsModule,
        ],
        providers: [
          {
            provide: ListingService,
            useValue: {
              findModels: (...args) => of({ docs: [] }),
              postRequest: (...args) => of({}),
            } as ListingService,
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(TenantListPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
