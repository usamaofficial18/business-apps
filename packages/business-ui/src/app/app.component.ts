import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, HostBinding, NgZone } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { App } from '@capacitor/app';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../environments/environment';
import { SET_ITEM, StorageService } from './auth/storage/storage.service';
import { LOGGED_IN, SELECTED_TENANT } from './auth/token/constants';
import { TokenService } from './auth/token/token.service';
import {
  ADMINISTRATOR,
  DARK_MODE,
  ON,
  OFF,
  X_TENANT_ID,
  DARK_MODE_CLASS_NAME,
} from './constants/strings';
import {
  GET_TENANT_USER_PROFILE_FOR_USER,
  TENANT_LIST_FOR_USER,
} from './constants/url-strings';
import { SetupPage } from './setup/setup.page';
import { ChooseTenantComponent } from './shared/components/choose-tenant/choose-tenant.component';
import { ListingService } from './shared/components/data-list/listing.service';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));
  loggedIn: boolean;
  cap = App;
  @HostBinding() class = '';
  toggleControl = new FormControl(false);
  isAdmin = false;
  isTenantAdmin = false;
  roles = [];
  tenantRoles = [];
  tenantName = '';
  tenantId = '';

  constructor(
    private breakpointObserver: BreakpointObserver,
    private readonly token: TokenService,
    private readonly store: StorageService,
    private readonly list: ListingService,
    private readonly router: Router,
    private readonly zone: NgZone,
    private readonly overlay: OverlayContainer,
    private readonly dialog: MatDialog,
  ) {}

  ngOnInit() {
    this.initializeApp();
    if (this.loggedIn) {
      this.loadProfile();
      this.loadTenant();
    }

    this.store.getItem(LOGGED_IN).then(loggedIn => {
      this.loggedIn = loggedIn === 'true';
      if (this.loggedIn) {
        this.loadProfile();
        this.loadTenant();
      }
    });

    this.store.getItem(DARK_MODE).then(darkMode => {
      this.setTheme(darkMode === ON ? true : false);
      this.toggleDarkMode(darkMode === ON ? true : false);
    });

    this.store.changes.subscribe({
      next: res => {
        if (res.event === SET_ITEM && res.value?.key === LOGGED_IN) {
          this.loggedIn = res.value?.value === 'true';
          if (!this.loggedIn) {
            this.isAdmin = false;
            this.roles = [];
            this.router.navigate(['/home']).then(navigated => {});
          }
          if (this.loggedIn) {
            this.loadProfile();
            this.loadTenant();
          }
        }
      },
      error: error => {},
    });
  }

  setTheme(darkMode: boolean) {
    this.saveTheme(darkMode);
    if (darkMode) {
      this.class = DARK_MODE_CLASS_NAME;
      this.overlay.getContainerElement().classList.add(this.class);
    } else {
      this.class = '';
      this.overlay.getContainerElement().classList.remove(DARK_MODE_CLASS_NAME);
    }
  }

  toggleDarkMode(darkMode: boolean) {
    if (darkMode) {
      this.toggleControl.setValue(true);
    } else {
      this.toggleControl.setValue(false);
    }
  }

  saveTheme(darkMode: boolean) {
    if (darkMode) {
      this.store.setItem(DARK_MODE, ON);
    } else {
      this.store.setItem(DARK_MODE, OFF);
    }
  }

  initializeApp() {
    this.cap.addListener('appUrlOpen', (data: any) => {
      this.zone.run(() => {
        const slug = data.url.split(`${environment.callbackProtocol}://`).pop();
        if (slug) {
          this.router.navigateByUrl(slug);
        }
      });
    });
  }

  logIn() {
    this.token.logIn();
  }

  chooseAccount() {
    this.token.logOut().then(() => this.token.logIn());
  }

  logOut() {
    this.token.logOut();
  }

  loadProfile() {
    this.token.loadProfile().subscribe({
      next: profile => {
        this.roles = profile?.roles;
        this.isAdmin = this.roles.includes(ADMINISTRATOR);
      },
      error: error => {},
    });
  }

  loadTenantUser() {
    this.list
      .getRequest(
        environment.adminServerUrl + GET_TENANT_USER_PROFILE_FOR_USER,
        undefined,
        { [X_TENANT_ID]: this.tenantId },
      )
      .subscribe({
        next: profile => {
          this.tenantRoles = profile?.tenantRoles;
          this.isTenantAdmin = this.tenantRoles.includes(ADMINISTRATOR);
        },
        error: error => {},
      });
  }

  switchTeam() {
    const dialogRef = this.dialog.open(ChooseTenantComponent, {
      width: '100vh',
    });

    dialogRef.afterClosed().subscribe({
      next: choice => {
        if (choice?.uuid) {
          this.tenantId = choice.uuid;
          this.tenantName = choice.tenantName;
          this.setTenantInStore(choice);
        }
        if (choice?.clear) {
          this.clearTenant();
        }
      },
    });
  }

  clearTenant() {
    this.store.removeItem(SELECTED_TENANT);
  }

  loadTenant() {
    this.store.getItem(SELECTED_TENANT).then(tenant => {
      if (tenant) {
        return this.parseTenant(tenant);
      }
      return this.openDialog();
    });

    this.store.changes.subscribe({
      next: res => {
        if (res.event === SET_ITEM && res.value?.key === SELECTED_TENANT) {
          this.parseTenant(res.value?.value);
        }
      },
      error: error => {},
    });
  }

  parseTenant(tenantString: string) {
    const tenant = JSON.parse(tenantString || '{}');
    this.tenantId = tenant.tenantId;
    this.tenantName = tenant.tenantName;
  }

  fetchFirstTenant() {
    this.list
      .findModels(environment.adminServerUrl + TENANT_LIST_FOR_USER)
      .pipe(map(res => (res?.docs?.length ? res?.docs[0] : {})))
      .subscribe({
        next: res => {
          this.tenantId = res.uuid;
          this.tenantName = res.tenantName;
          this.setTenantInStore(res);
        },
      });
  }

  openDialog() {
    this.dialog.open(SetupPage, {
      disableClose: true,
      width: '350px',
      autoFocus: false,
    });
  }

  setTenantInStore(tenant: { uuid: string; tenantName: string }) {
    this.store.setItem(
      SELECTED_TENANT,
      JSON.stringify({
        tenantId: tenant.uuid,
        tenantName: tenant.tenantName,
      }),
    );
  }
}
