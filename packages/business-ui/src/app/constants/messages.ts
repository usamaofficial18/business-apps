export const SOMETHING_WENT_WRONG = 'Something went wrong!';
export const DELETING = 'Deleting...';
export const UPPERCASE_DELETE = 'DELETE';
export const CONFIRM_DELETE_ENTITY =
  'Are you sure you want to delete the entity?';
export const TYPE_DELETE_TO_PROCEED = 'Type DELETE to proceed.';
export const CANNOT_DELETE = 'Cannot Delete';
export const TENANT = 'Tenant';
export const CONFIRM_DELETE_TENANT =
  'Are you sure you want to delete the tenant?';
export const UPDATED = 'Updated';
export const CANNOT_UPDATE = 'Cannot Update';
export const JOURNAL_ENTRY = 'Journal Entry';
export const CONFIRM_DELETE_JOURNAL_ENTRY =
  'Are you sure you want to delete the journal entry?';
export const CANNOT_CREATE = 'Cannot Create';
