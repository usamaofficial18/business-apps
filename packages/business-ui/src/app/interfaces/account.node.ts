export interface AccountNode {
  uuid: string;
  tenantId: string;
  accountingEntity: string;
  currency: string;
  parentAccount: string;
  parentAccountNumber: string;
  accountName: { en: string; ar?: string };
  accountNumber: string;
  level: number;
  children?: AccountNode[];
  expandable: boolean;
  balanceType: string;
  classification: string;
}
