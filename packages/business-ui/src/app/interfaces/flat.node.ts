export interface FlatNode {
  expandable: boolean;
  level: number;
  isLoading?: boolean;
  uuid: string;
  tenantId: string;
  accountingEntity: string;
  currency: string;
  parentAccount: string;
  parentAccountNumber: string;
  accountName: { en: string; ar?: string };
  accountNumber: string;
  balanceType: string;
  classification: string;
}
