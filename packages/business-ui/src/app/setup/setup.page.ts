import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, throwError } from 'rxjs';
import {
  startWith,
  debounceTime,
  switchMap,
  map,
  catchError,
  filter,
} from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AddEntityComponent } from '../account/accounting-entity/add-entity/add-entity.component';
import { StorageService } from '../auth/storage/storage.service';
import {
  DEFAULT_ACCOUNTING_ENTITY,
  SELECTED_TENANT,
} from '../auth/token/constants';
import { SOMETHING_WENT_WRONG } from '../constants/messages';
import { UNDO_DURATION, X_TENANT_ID } from '../constants/strings';
import {
  CREATE_ACCOUNTING_ENTITY,
  LIST_ACCOUNTING_ENTITIES,
  TENANT_LIST_FOR_USER,
} from '../constants/url-strings';
import { ValidateInputSelected } from '../pipes/validators';
import { ListingService } from '../shared/components/data-list/listing.service';

@Component({
  selector: 'app-setup',
  templateUrl: './setup.page.html',
  styleUrls: ['./setup.page.scss'],
})
export class SetupPage implements OnInit {
  setupForm = new FormGroup({
    tenantControl: new FormControl('', [Validators.required]),
    entityControl: new FormControl(''),
  });

  tenantId = '';
  tenants: Observable<any>;
  accountingEntities: Observable<any>;
  showEntity = false;
  headers = {};
  validateInput: any = ValidateInputSelected;

  constructor(
    private readonly dialog: MatDialog,
    private readonly list: ListingService,
    public store: StorageService,
    private readonly dialogRef: MatDialogRef<AddEntityComponent>,
    public snackBar: MatSnackBar,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}

  ngOnInit() {
    this.tenants = this.setupForm.get('tenantControl').valueChanges.pipe(
      startWith(''),
      debounceTime(300),
      switchMap(tenantName =>
        this.list.findModels(
          environment.adminServerUrl + TENANT_LIST_FOR_USER,
          { tenantName },
        ),
      ),
      map(res => res.docs),
    );
  }

  get f() {
    return this.setupForm.controls;
  }

  selectTenant() {
    const choice = this.f.tenantControl.value;
    if (choice?.uuid) {
      this.tenantId = choice.uuid;
      this.headers = { [X_TENANT_ID]: this.tenantId };
      this.setEntityAutoComplete();
      this.showEntity = true;
    }
    if (choice?.clear) {
      this.clearTenant();
    }
  }

  setEntityAutoComplete() {
    this.accountingEntities = this.setupForm
      .get('entityControl')
      .valueChanges.pipe(
        startWith(''),
        debounceTime(300),
        switchMap(val => {
          return this.list.findModels(
            environment.accountsServerUrl + LIST_ACCOUNTING_ENTITIES,
            { name: val },
            undefined,
            undefined,
            5,
            this.headers,
          );
        }),
        map(r => r.docs),
        catchError(error => {
          return throwError(error);
        }),
      );
  }

  clearTenant() {
    this.store.removeItem(SELECTED_TENANT);
  }

  submitForm() {
    const tenant = this.f.tenantControl.value;
    if (tenant?.uuid) {
      const entity = this.f.entityControl.value;
      this.setTenantInStore(tenant);
      this.setEntityInStore(entity);
      this.dialogRef.close();
    }
  }

  setTenantInStore(tenant: { uuid: string; tenantName: string }) {
    this.store.setItem(
      SELECTED_TENANT,
      JSON.stringify({
        tenantId: tenant.uuid,
        tenantName: tenant.tenantName,
      }),
    );
  }

  setEntityInStore(entity: { name: string; uuid: string }) {
    this.store.setItem(
      DEFAULT_ACCOUNTING_ENTITY,
      JSON.stringify({
        name: entity.name,
        uuid: entity.uuid,
      }),
    );
  }

  openAddAccountingEntityDialog() {
    this.f.entityControl.disable();
    const dialogRef = this.dialog.open(AddEntityComponent, {
      width: '100vh',
      data: {},
    });

    dialogRef.afterOpened().subscribe({
      next: res => {
        this.f.entityControl.enable();
      },
    });

    dialogRef
      .afterClosed()
      .pipe(
        filter(res => res instanceof FormGroup),
        switchMap((form: FormGroup) => {
          return this.list.postRequest(
            environment.accountsServerUrl + CREATE_ACCOUNTING_ENTITY,
            {
              name: form.controls.name.value,
              entityType: form.controls.entityType.value,
              taxId: form.controls.taxId.value,
              baseCurrency: form.controls.baseCurrency.value,
              country: form.controls.country.value,
              abbr: form.controls.abbr.value,
              dateOfBirth: form.controls.dateOfBirth.value,
            },
            this.headers,
          );
        }),
      )
      .subscribe({
        next: res => {
          this.setEntityAutoComplete();
        },
        error: error => {
          this.snackBar.open(
            error?.message || SOMETHING_WENT_WRONG,
            undefined,
            { duration: UNDO_DURATION },
          );
        },
      });
  }

  displayTenantName(tenant: { tenantName: string }) {
    return tenant.tenantName;
  }

  displayEntityName(entity?: { name: string; uuid: string }) {
    return entity?.name;
  }
}
