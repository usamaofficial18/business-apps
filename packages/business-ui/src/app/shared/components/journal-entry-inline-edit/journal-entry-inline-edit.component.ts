import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, Host, Input, OnInit, Optional } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { SatPopover } from '@ncstate/sat-popover';
import { Observable } from 'rxjs';
import { debounceTime, map, startWith, switchMap } from 'rxjs/operators';
import { StorageService } from '../../../auth/storage/storage.service';
import { DARK_MODE, ON } from '../../../constants/strings';
import { LIST_ACCOUNTS } from '../../../constants/url-strings';
import { environment } from '../../../../environments/environment';
import { ListingService } from '../data-list/listing.service';

@Component({
  selector: 'journal-entry-inline-edit',
  templateUrl: './journal-entry-inline-edit.component.html',
  styleUrls: ['./journal-entry-inline-edit.component.scss'],
})
export class JournalEntryInlineEditComponent implements OnInit {
  filteredAccountList: Observable<any>;
  accountFormControl = new FormControl('', [Validators.required]);
  debitFormControl = new FormControl();
  creditFormControl = new FormControl();
  debitInAccountCurrencyFormControl = new FormControl();
  creditInAccountCurrencyFormControl = new FormControl();
  transactionLinkFormControl = new FormControl();
  remarkFormControl = new FormControl();
  private _value = '';

  @Input()
  get value(): any {
    return this._value;
  }

  set value(x) {
    this._value = x;
  }

  @Input()
  column: string;

  constructor(
    @Optional() @Host() public popover: SatPopover,
    private readonly listingService: ListingService,
    private readonly storage: StorageService,
    private overlayContainer: OverlayContainer,
  ) {}

  ngOnInit() {
    this.storage.getItem(DARK_MODE).then(darkMode => {
      this.overlayContainer
        .getContainerElement()
        .classList.add(darkMode === ON ? 'darkMode' : 'noop');
    });
    this.getAccountsList();
  }

  getAccountsList() {
    this.filteredAccountList = this.accountFormControl.valueChanges.pipe(
      startWith(''),
      debounceTime(300),
      switchMap(value => {
        let filters: any = {};
        if (value) {
          filters = {
            'accountName.en': value ? value : filters,
          };
        }

        return this.listingService.findModels(
          environment.accountsServerUrl + LIST_ACCOUNTS,
          filters,
        );
      }),
      map(res => res.docs),
    );
  }

  getAccountName(account) {
    return account?.accountNameEn;
  }

  onSubmit() {
    if (this.popover) {
      const account: any = {};
      if (this.column === 'account') {
        account.accountNameEn = this.accountFormControl.value.accountNameEn;
        account.account = this.accountFormControl.value.uuid;
      }
      if (this.column === 'debit') {
        account.debit = this.debitFormControl.value;
      }
      if (this.column === 'credit') {
        account.credit = this.creditFormControl.value;
      }
      if (this.column === 'creditInAccountCurrency') {
        account.creditInAccountCurrency =
          this.creditInAccountCurrencyFormControl.value;
      }
      if (this.column === 'debitInAccountCurrency') {
        account.debitInAccountCurrency =
          this.debitInAccountCurrencyFormControl.value;
      }
      if (this.column === 'transactionLink') {
        account.transactionLink = this.transactionLinkFormControl.value;
      }
      if (this.column === 'remark') {
        account.remark = this.remarkFormControl.value;
      }
      this.popover.close(account);
    }
  }

  onCancel() {
    if (this.popover) {
      this.popover.close();
    }
  }
}
