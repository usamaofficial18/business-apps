import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material.module';
import { TitleCaseFilterPipe } from '../pipes/title-case-filter.pipe';
import { ChooseTenantComponent } from './components/choose-tenant/choose-tenant.component';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { DataListComponent } from './components/data-list/data-list.component';
import { JournalEntryInlineEditComponent } from './components/journal-entry-inline-edit/journal-entry-inline-edit.component';

@NgModule({
  declarations: [
    DataListComponent,
    ConfirmationDialogComponent,
    ChooseTenantComponent,
    JournalEntryInlineEditComponent,
    TitleCaseFilterPipe,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
  ],
  exports: [
    DataListComponent,
    ConfirmationDialogComponent,
    ChooseTenantComponent,
    JournalEntryInlineEditComponent,
  ],
})
export class SharedModule {}
