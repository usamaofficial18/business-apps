import { EventLogModule } from '@castlecraft/event-log';
import { Module } from '@nestjs/common';
import { ConfigModule } from './config/config.module';
import {
  ConfigService,
  DB_HOST,
  DB_NAME,
  DB_PASSWORD,
  DB_USER,
  MONGO_URI_PREFIX,
} from './config/config.service';
import { Controllers } from './controllers';

@Module({
  imports: [
    ConfigModule,
    EventLogModule.registerAsync({
      useFactory: (config: ConfigService) => ({
        mongoUriPrefix: config.get(MONGO_URI_PREFIX) || 'mongodb',
        eventsDbHost: config.get(DB_HOST),
        eventsDbUser: config.get(DB_USER),
        eventsDbPassword: config.get(DB_PASSWORD),
        eventsDbName: config.get(DB_NAME),
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [...Controllers],
})
export class AppModule {}
