FROM node:latest
# Copy app
COPY . /home/craft/identity-store
WORKDIR /home/craft/
RUN npm config set @castlecraft:registry https://gitlab.com/api/v4/projects/28015527/packages/npm/ \
    && cd identity-store \
    && yarn \
    && yarn build \
    && yarn --production=true

FROM node:slim
# Install dependencies
RUN apt-get update \
    && apt-get install -y gettext-base wait-for-it \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Setup docker-entrypoint
COPY docker/docker-entrypoint.sh usr/local/bin/docker-entrypoint.sh
RUN ln -s usr/local/bin/docker-entrypoint.sh / # backwards compat

# Add non root user
RUN useradd -ms /bin/bash craft
WORKDIR /home/craft/identity-store
COPY --from=0 /home/craft/identity-store .

RUN chown -R craft:craft /home/craft

# set project directory
WORKDIR /home/craft/identity-store

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["start"]
