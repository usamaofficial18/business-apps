import { TenantUserService, UserService } from '@castlecraft/auth';
import { Controller } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { EventPayload } from '../../interfaces/event-payload.interface';

export const UserAccountAddedEvent = 'UserAccountAddedEvent';
export const UserAccountModifiedEvent = 'UserAccountModifiedEvent';
export const UserAccountRemovedEvent = 'UserAccountRemovedEvent';
export const PhoneVerifiedEvent = 'PhoneVerifiedEvent';
export const EmailVerifiedAndUpdatedEvent = 'EmailVerifiedAndUpdatedEvent';
export const EmailVerifiedAndPasswordSetEvent =
  'EmailVerifiedAndPasswordSetEvent';
export const UserSignedUpViaEmailEvent = 'UserSignedUpViaEmailEvent';

@Controller('users')
export class UsersController {
  constructor(
    private readonly user: UserService,
    private readonly tenantUser: TenantUserService,
  ) {}

  @EventPattern(UserAccountAddedEvent)
  userAccountAdded(payload: EventPayload) {
    this.user
      .findOne({ uuid: payload?.eventData?.user?.uuid })
      .then(user => {
        if (!user) {
          return this.user.insertOne(payload?.eventData?.user);
        }
      })
      .then(savedUser => {})
      .catch(err => {});
  }

  @EventPattern(UserAccountModifiedEvent)
  userAccountModified(payload: EventPayload) {
    delete payload?.eventData?.user?._id;
    this.user
      .findOne({ uuid: payload?.eventData?.user?.uuid })
      .then(user => {
        if (!user) {
          return this.user.insertOne(payload?.eventData?.user);
        }
        return this.user
          .updateOne(
            { uuid: payload?.eventData?.user?.uuid },
            { $set: payload?.eventData?.user },
          )
          .then(savedUser => user);
      })
      .then(savedUser => {})
      .catch(err => {});
  }

  @EventPattern(UserAccountRemovedEvent)
  userAccountRemoved(payload: EventPayload) {
    this.user
      .deleteOne({ uuid: payload?.eventData?.deletedUser?.uuid })
      .then(userRemoved =>
        this.tenantUser.deleteMany({
          user: payload?.eventData?.deletedUser?.uuid,
        }),
      )
      .then(tenantUserRemoved => {})
      .catch(err => {});
  }

  @EventPattern(PhoneVerifiedEvent)
  phoneVerified(payload: EventPayload) {
    delete payload?.eventData?.user?._id;
    this.user
      .findOne({ uuid: payload?.eventData?.user?.uuid })
      .then(user => {
        if (!user) {
          return this.user.insertOne(payload?.eventData?.user);
        }
        return this.user
          .updateOne(
            { uuid: payload?.eventData?.user?.uuid },
            { $set: payload?.eventData?.user },
          )
          .then(savedUser => user);
      })
      .then(savedUser => {})
      .catch(err => {});
  }

  @EventPattern(EmailVerifiedAndUpdatedEvent)
  emailVerifiedAndUpdated(payload: EventPayload) {
    delete payload?.eventData?.verifiedUser?._id;
    this.user
      .findOne({ uuid: payload?.eventData?.verifiedUser?.uuid })
      .then(user => {
        if (!user) {
          return this.user.insertOne(payload?.eventData?.verifiedUser);
        }
        return this.user
          .updateOne(
            { uuid: payload?.eventData?.verifiedUser?.uuid },
            { $set: payload?.eventData?.verifiedUser },
          )
          .then(savedUser => user);
      })
      .then(savedUser => {})
      .catch(err => {});
  }

  @EventPattern(EmailVerifiedAndPasswordSetEvent)
  emailVerifiedAndPasswordSet(payload: EventPayload) {
    delete payload?.eventData?.verifiedUser?._id;
    this.user
      .findOne({ uuid: payload?.eventData?.verifiedUser?.uuid })
      .then(user => {
        if (!user) {
          return this.user.insertOne(payload?.eventData?.verifiedUser);
        }
        return this.user
          .updateOne(
            { uuid: payload?.eventData?.verifiedUser?.uuid },
            { $set: payload?.eventData?.verifiedUser },
          )
          .then(savedUser => user);
      })
      .then(savedUser => {})
      .catch(err => {});
  }

  @EventPattern(UserSignedUpViaEmailEvent)
  userSignedUpViaEmail(payload: EventPayload) {
    delete payload?.eventData?.unverifiedUser?._id;
    this.user
      .findOne({ uuid: payload?.eventData?.unverifiedUser?.uuid })
      .then(user => {
        if (!user) {
          return this.user.insertOne(payload?.eventData?.unverifiedUser);
        }
        return this.user
          .updateOne(
            { uuid: payload?.eventData?.unverifiedUser?.uuid },
            { $set: payload?.eventData?.unverifiedUser },
          )
          .then(savedUser => user);
      })
      .then(savedUser => {})
      .catch(err => {});
  }
}
