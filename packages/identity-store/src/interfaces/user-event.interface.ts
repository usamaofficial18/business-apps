export interface User {
  _id?: string;
  uuid?: string;
  creation?: Date;
  modified?: Date;
  createdBy?: string;
  modifiedBy?: string;
  disabled?: boolean;
  name?: string;
  phone?: string;
  email?: string;
  password?: string;
  roles?: string[];
  enable2fa?: boolean;
  sharedSecret?: string;
  otpPeriod?: number;
  otpCounter?: string;
  twoFactorTempSecret?: string;
  deleted?: boolean;
  enablePasswordLess?: boolean;
  unverifiedPhone?: string;
  isEmailVerified?: boolean;
}

export interface AuthData {
  uuid?: string;
  password?: string;
  metaData?: { [key: string]: string | number };
  entity?: string;
  entityUuid?: string;
  expiry?: Date;
  authDataType?: string;
}

export interface UserEventData {
  user?: User;
  authData?: AuthData;
  phoneOTP?: AuthData;
  verificationCode?: AuthData;
  deletedUser?: User;
  password?: AuthData;
  sharedSecret?: AuthData;
  otpCounter?: AuthData;
  twoFactorTempSecret?: AuthData;
  email?: string;
  name?: string;
  socialLogin?: string;
  verifiedUser?: User;
  unverifiedUser?: User;
  verifiedEmail?: AuthData;
  userPassword?: AuthData;
}
