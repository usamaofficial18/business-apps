import { NestFactory } from '@nestjs/core';
import { addTenantRoles } from './add-tenant-roles';
import { AppModule } from './app.module';
import { setupEvents } from './events-server';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  setupEvents(app);
  await addTenantRoles(app);
}
bootstrap();
