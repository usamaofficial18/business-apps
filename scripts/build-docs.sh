#!/usr/bin/env bash
echo "Building Accounts Server Docs"
./node_modules/.bin/compodoc \
    -d ./public/api/accounts-server \
    -p packages/accounts-server/tsconfig.json \
    --name "Accounts Server"

echo "Building Admin Server Docs"
./node_modules/.bin/compodoc \
    -d ./public/api/admin-server \
    -p packages/admin-server/tsconfig.json \
    --name "Admin Server"

echo "Building Event Store Docs"
./node_modules/.bin/compodoc \
    -d ./public/api/event-store \
    -p packages/event-store/tsconfig.json \
    --name "Event Store"

echo "Building Identity Store Docs"
./node_modules/.bin/compodoc \
    -d ./public/api/identity-store \
    -p packages/identity-store/tsconfig.json \
    --name "Identity Store"

echo "Building Event Log Library Docs"
./node_modules/.bin/compodoc \
    -d ./public/api/event-log \
    -p libraries/event-log/tsconfig.json \
    --name "Event Log"

echo "Building Auth Libary Docs"
./node_modules/.bin/compodoc \
    -d ./public/api/auth \
    -p libraries/auth/tsconfig.json \
    --name "Auth Libary"
